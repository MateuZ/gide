#ifndef __UTILS__
#define __UTILS__

/* C++ includes */
#include <iostream>
#include <chrono>
#include <functional>
#include <iomanip>
#include <random>
#include <algorithm>
#include <tuple>
#include <fstream>

/* C includes */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <assert.h>
#include <string.h>

#define INFO            0
#define INIT_PRINT      0
#define INIT_PRINT_C	0
#define KERNEL_PRINT    0
#define INDEX_GENERATOR 0
#define MIGRATION_PRINT 0

#define SAVE_STATS      1

#define POP_SIZE    300
#define N_GER      1000
#define N_ISLANDS     1
#define F           0.5
#define CR		    0.9
#define DIM		   1000

typedef std::tuple<double, double, double> tuple_3d;
typedef std::vector< tuple_3d > v_tuple_3d;
typedef std::vector< int > vi;
typedef std::vector< vi > vvi;

typedef std::vector< double > vd;
typedef std::vector< vd > vvd;

double random_btw_double_uniform_distribution(const double a, const double b);
int rand_btw(int a, int b);

#endif

