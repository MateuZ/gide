#ifndef __F_ROSENBROCK__
#define __F_ROSENBROCK__

#define SPHERE_PRINT 0

#include "utils.hpp"

class Rosenbrock {
	public:
		int D;     //dimension
		double LB; //lower-bound
		double UB; //upper-bound
		double *x; //vector with the variables, size D;
		double evaluation_value;

		Rosenbrock( );

		Rosenbrock( int, double, double );
		~Rosenbrock();

		void operator-=( const Rosenbrock& b )
		{
			assert(D  == b.D );
			assert(LB == b.LB);
			assert(UB == b.UB);

			for(int i = 0; i < D; i++)
				x[i] = x[i] - b.x[i];
		}

		Rosenbrock& operator=( const Rosenbrock& b )
		{
			D  = b.D;
			LB = b.LB;
			UB = b.UB;

			x = new double[D];

			for( int i = 0; i < D; i++ )
				x[i] = b.x[i];

			return *this;
		}

		void operator*=( const double& FM )
		{
			for( int i = 0; i < D; i++ )
				x[i] *= FM;
		}

		void operator+=( const Rosenbrock& b )
		{
			assert(D  == b.D );
			assert(LB == b.LB);
			assert(UB == b.UB);

			for(int i = 0; i < D; i++)
				x[i] = x[i] + b.x[i];
		}

		double evaluate();
		
		void print();
		void init( int, double, double );
		void clean_init( int, double, double );

		void verify_bounds();

		void copy_data( Rosenbrock& b );

    private:
		
};


#endif
