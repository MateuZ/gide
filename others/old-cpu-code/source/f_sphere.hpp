#ifndef __F_SPHERE__
#define __F_SPHERE__

#define SPHERE_PRINT 0

#include "utils.hpp"

class SphereFunction {
	public:
		int D;     //dimension
		double LB; //lower-bound
		double UB; //upper-bound
		double *x; //vector with the variables, size D;
		double max_value;

		SphereFunction( );
		
		//SphereFunction(const SphereFunction& b);

		SphereFunction( int, double, double );
		~SphereFunction();

		void operator-=( const SphereFunction& b )
		{
			assert(D  == b.D );
			assert(LB == b.LB);
			assert(UB == b.UB);

			for(int i = 0; i < D; i++)
				x[i] = x[i] - b.x[i];
		}

		SphereFunction& operator=( const SphereFunction& b )
		{
			D  = b.D;
			LB = b.LB;
			UB = b.UB;
			fitness_value = b.fitness_value;

			x = new double[D];

			max_value = UB * UB * D;

			for( int i = 0; i < D; i++ )
				x[i] = b.x[i];

			return *this;
		}

		void operator*=( const double& FM )
		{
			for( int i = 0; i < D; i++ )
				x[i] *= FM;
		}

		void operator+=( const SphereFunction& b )
		{
			assert(D  == b.D );
			assert(LB == b.LB);
			assert(UB == b.UB);

			for(int i = 0; i < D; i++)
				x[i] = x[i] + b.x[i];
		}

		bool operator<(const SphereFunction & other) //(1)
    	{
        	return fitness_value < other.fitness_value;
    	}

		double evaluate();
		double fitness( double );

		
		void print();
		void init( int, double, double );
		void clean_init( int, double, double );

		void verify_bounds();
		void setFitnessValue( double value );
		/* Since the type of this in such case is const, no modifications of member variables are possible. */
		double getFitnessValue() const;
		double getDimensionValue( int i );

		void copy_data( SphereFunction& b );

		void do_operations( const SphereFunction& a, const SphereFunction& b, const SphereFunction& c );

	private:
		double fitness_value;
		
};


#endif
