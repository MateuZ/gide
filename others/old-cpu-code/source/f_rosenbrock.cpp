#include "f_rosenbrock.hpp"
#include "utils.hpp"

Rosenbrock::Rosenbrock()
{
	/* empty */
}

Rosenbrock::Rosenbrock( int _D, double _LB, double _UB ):
	D(_D),
	LB(_LB),
	UB(_UB)
{
	x = new double[D];

	for( int i = 0; i < D; i++ )
		x[i] = random_btw_double_uniform_distribution( LB, UB );

	#if SPHERE_PRINT == 1 
		printf("New Rosenbrock Function defined with %d dimensions\n", D);
		printf("Lower-Bound: %.3lf\n", LB);
		printf("Upper-Bound: %.3lf\n", UB);
	#endif
}

void Rosenbrock::copy_data( Rosenbrock& b ){
	D  = b.D;
	LB = b.LB;
	UB = b.UB;
	
	for( int i = 0; i < D; i++ )
		x[i] = b.x[i];
}

void Rosenbrock::init( int _D, double _LB, double _UB )
{
	D  = _D;
	LB = _LB;
	UB = _UB;

	x = new double[D];

	for( int i = 0; i < D; i++ ){
		x[i] = random_btw_double_uniform_distribution( LB, UB );
	}

	#if INIT_PRINT == 1
		std::cout << "Rosenbrock function defined with " << D << " dimensions" << std::endl;
		std::cout << "Lower-Bound: " << LB << std::endl;
		std::cout << "Upper-Bound: " << UB << std::endl;
		for( int i = 0; i < D; i++ ){
			std::cout << std::setprecision(3) << std::fixed << x[i] << " ";
		}
		std::cout << std::endl;
	#endif
}

void Rosenbrock::clean_init( int _D, double _LB, double _UB )
{
	D  = _D;
	LB = _LB;
	UB = _UB;

	x = new double[D];
	std::fill_n(x, D, static_cast<double>(0.0f));

	#if INIT_PRINT_C == 1
		std::cout << "Rosenbrock Function defined with " << D << " dimensions" << std::endl;
		std::cout << "Lower-Bound: " << LB << std::endl;
		std::cout << "Upper-Bound: " << UB << std::endl;
	#endif
}

Rosenbrock::~Rosenbrock(void)
{
	//std::cout << "Deleting object" << std::endl;
	if( x != NULL ){
		delete [] x;
		x = NULL;
	}
}

void Rosenbrock::print()
{
	std::cout << "Rosenbrock Function defined with " << D << " dimensions" << std::endl;
	std::cout << "Lower-Bound: " << LB << std::endl;
	std::cout << "Upper-Bound: " << UB << std::endl;

	for( int i = 0; i < D; i++ ){
		std::cout << std::setprecision(3) << std::fixed << x[i] << " ";
	}
	std::cout << std::endl;
}

double Rosenbrock::evaluate()
{
	double sum = 0.0f;

	for(unsigned short int i = 0; i < D-1; i++ ){
		sum += 100.0*( ( x[i+1] - (x[i]*x[i]) ) * ( x[i+1] - (x[i]*x[i]) ) ) - ( ( x[i] - 1 ) * (x[i] - 1) );
	}
	evaluation_value = sum;
	return sum;
}

void Rosenbrock::verify_bounds()
{
	for( int i = 0; i < D; i++ )
	{
		if( x[i] > UB )
			x[i] = UB;
		
		if( x[i] < LB )
			x[i] = LB;
	}
}
