#include "f_sphere.hpp"
#include "f_rosenbrock.hpp"

typedef std::vector< SphereFunction > vSF;
typedef std::vector< vSF > vvSF;

double random_btw_double_uniform_distribution(const double a, const double b)
{
	std::mt19937 rng; 
    rng.seed(std::random_device{}());
	std::uniform_real_distribution<double> d(a, b);
	double real_rand = d(rng);
	return real_rand;
}

int rand_btw(int a, int b)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> dist(a, b);
	return dist(gen);
}

void differential_evolution(vvi& vf, vvSF& pop, vvSF& aux ){

	int n1, n2, n3;
	
	int rnbr;

	for( int i = 0; i < N_ISLANDS; i++ ){
		for( int j = 0; j < POP_SIZE; j++ ){

			rnbr = rand_btw(0, DIM-1);

			n1 = vf[i][j];
			n2 = vf[i][j + POP_SIZE];
			n3 = vf[i][j + 2 * POP_SIZE];
			
			//crossover!
			for( int k = 0; k < DIM; k++ ){

				if( (random_btw_double_uniform_distribution(0.0, 1.0) <= CR) or (k == rnbr) ){
					//receive a mutation dimension;
					aux[i][j].x[k] = pop[i][n1].x[k] + F * (pop[i][n2].x[k] - pop[i][n3].x[k]);
					
					if( aux[i][j].x[k] > aux[i][j].UB )
						aux[i][j].x[k] = aux[i][j].UB;

					if( aux[i][j].x[k] < aux[i][j].LB )
						aux[i][j].x[k] = aux[i][j].LB;

				}else{
					//receive an equal dimension;
					aux[i][j].x[k] = pop[i][j].x[k];
				}
			}
			
			aux[i][j].fitness( aux[i][j].evaluate() );
		}
	}
}

void subs_better_offsprings( vvSF& pop, vvSF& aux, v_tuple_3d& stats ){
	
	double best_f = 0.0f, worst_f = 1.0f, avg_f = 0.0f, accumulator = 0.0f, temp;

	for( int i = 0; i < N_ISLANDS; i++ ){
		for( int j = 0; j < POP_SIZE; j++ ){
			if(aux[i][j].getFitnessValue() > pop[i][j].getFitnessValue()){
				#if KERNEL_PRINT == 1
					printf("Island: [#%d] Ind: [%d] subs -> $%.10lf x $%.10lf\n", i, j, aux[i][j].getFitnessValue(), pop[i][j].getFitnessValue());
				#endif
				pop[i][j].copy_data( aux[i][j] );
			}
			temp = pop[i][j].getFitnessValue();
			if(temp > best_f)
				best_f = temp;

			if(temp < worst_f)
				worst_f = temp;

			accumulator += temp;
		}
	}
	avg_f = accumulator / (N_ISLANDS * POP_SIZE);
	stats.push_back(std::make_tuple(best_f, avg_f, worst_f));
}

void calc_fitness_SF(vvSF& pop, v_tuple_3d& stats){
	int k;
	double sum = 0.0f;
	double best_f = 0.0f, worst_f = 1.0f, avg_f = 0.0f, aux = 0.0f, fitness;
	for( int i = 0; i < N_ISLANDS; i++ ){
		for( int j = 0; j < POP_SIZE; j++ ){
			sum = 0.0f;
			for( k = 0; k < pop[i][j].D; k++)
				sum += (pop[i][j].x[k] * pop[i][j].x[k]);

			//std::cout << "[" << j << "]" << "$" << static_cast<double>(1.0f - static_cast<double>( sum / pop[i][j].max_value )) << std::endl;
			fitness = static_cast<double>(1.0f - static_cast<double>( sum / pop[i][j].max_value ));
			pop[i][j].setFitnessValue(fitness);			

			if(fitness > best_f)
				best_f = fitness;

			if(fitness < worst_f)
				worst_f = fitness;

			aux += fitness;
		}
	}
	avg_f = aux / (N_ISLANDS * POP_SIZE);

	
	stats.push_back(std::make_tuple(best_f, avg_f, worst_f));
}

void cpu_index_generator( vvi& v, vvi& vf  ){
	int n1, n2, n3;

	for( int i = 0; i < N_ISLANDS; i++ ){
		for( int j = 0; j < POP_SIZE; j++ ){
			
			n1 = rand() % POP_SIZE;
			//std::cout << "n1: " << n1 << std::endl;
			
			vf[i][j] = v[i][n1];
			
			if( v[i][n1] == j ){
				n1 = (n1 + 1) % POP_SIZE;
				vf[i][j] = v[i][n1];
			}

			n2 = rand() % static_cast<int>(POP_SIZE/3) + 1;
			//std::cout << "n2: " << n2 << std::endl;
			
			vf[i][j + POP_SIZE] = v[i][ (n1 + n2) % POP_SIZE ];

			if ( vf[i][j + POP_SIZE] == j){
				n2 = (n2 + 1) % POP_SIZE;
				vf[i][j + POP_SIZE] = v[i][ (n1 + n2) % POP_SIZE ];
			}

			//std::cout << "n2: " << n2 << std::endl;

			n3 = rand() % static_cast<int>(POP_SIZE/3) + 1;
			//std::cout << "n3: " << n3 << std::endl;

			vf[i][j + 2 * POP_SIZE] = v[i][ (n1 + n2 + n3) % POP_SIZE ];
			
			if( vf[i][j + 2 * POP_SIZE] == j ){
				n3 = ( n3 + 1 ) % POP_SIZE;
				vf[i][j + 2 * POP_SIZE] = v[i][ (n1 + n2 + n3) % POP_SIZE ];
			}
			//std::cout << "n3: " << n3 << std::endl;
		}
	}
}

/* ============ ROSENBROCK ================== */

typedef std::vector< Rosenbrock > vRosenbrock;
typedef std::vector< vRosenbrock > vvRosenbrock;

void evaluateRosenbrockPop(vvRosenbrock& pop, v_tuple_3d& stats){
	int k;
	double sum = 0.0f;
	double best_f = 0.0f, worst_f = 1.0f, avg_f = 0.0f, aux = 0.0f, fitness;
	for( int i = 0; i < N_ISLANDS; i++ ){
		for( int j = 0; j < POP_SIZE; j++ ){
			pop[i][j].evaluate();

			if(pop[i][j].evaluation_value < best_f || (i == 0 && j == 0))
				best_f = pop[i][j].evaluation_value;

			if(pop[i][j].evaluation_value > worst_f || (i == 0 && j == 0))
				worst_f = pop[i][j].evaluation_value;

			avg_f += (pop[i][j].evaluation_value/(N_ISLANDS * POP_SIZE));
		}
	}
	
	stats.push_back(std::make_tuple(best_f, avg_f, worst_f));
}


void differential_evolution_Rosenbrock(vvi& vf, vvRosenbrock& pop, vvRosenbrock& aux ){

	int n1, n2, n3;
	
	int rnbr;

	for( int i = 0; i < N_ISLANDS; i++ ){
		for( int j = 0; j < POP_SIZE; j++ ){

			rnbr = rand_btw(0, DIM-1);

			n1 = vf[i][j];
			n2 = vf[i][j + POP_SIZE];
			n3 = vf[i][j + 2 * POP_SIZE];
			
			//crossover!
			for( int k = 0; k < DIM; k++ ){

				if( (random_btw_double_uniform_distribution(0.0, 1.0) <= CR) or (k == rnbr) ){
					//receive a mutation dimension;
					aux[i][j].x[k] = pop[i][n1].x[k] + F * (pop[i][n2].x[k] - pop[i][n3].x[k]);
					
					if( aux[i][j].x[k] > aux[i][j].UB )
						aux[i][j].x[k] = aux[i][j].UB;

					if( aux[i][j].x[k] < aux[i][j].LB )
						aux[i][j].x[k] = aux[i][j].LB;

				}else{
					//receive an equal dimension;
					aux[i][j].x[k] = pop[i][j].x[k];
				}
			}
			
			aux[i][j].evaluate();
		}
	}
}

void subs_better_offsprings_Rosenbrock( vvRosenbrock& pop, vvRosenbrock& aux, v_tuple_3d& stats ){
	
	double best_f = 0.0f, worst_f = 1.0f, avg_f = 0.0f, temp;

	for( int i = 0; i < N_ISLANDS; i++ ){
		for( int j = 0; j < POP_SIZE; j++ ){
			if(aux[i][j].evaluation_value < pop[i][j].evaluation_value){
				#if KERNEL_PRINT == 1
					printf("Island: [#%d] Ind: [%d] subs -> $%.10lf x $%.10lf\n", i, j, aux[i][j].evaluation_value, pop[i][j].evaluation_value);
				#endif
				pop[i][j].copy_data( aux[i][j] );
			}
			temp = pop[i][j].evaluation_value;

			if(temp < best_f || (i == 0 && j == 0) )
				best_f = temp;

			if(temp > worst_f)
				worst_f = temp;

			avg_f += temp / (N_ISLANDS * POP_SIZE);
		}
	}
	stats.push_back(std::make_tuple(best_f, avg_f, worst_f));
}


int main(void)
{	
	srand(time(NULL));
	
	//vvRosenbrock pop(N_ISLANDS, vRosenbrock(POP_SIZE));
	//vvRosenbrock aux(N_ISLANDS, vRosenbrock(POP_SIZE));
	vvSF pop(N_ISLANDS, vSF(POP_SIZE));
	vvSF aux(N_ISLANDS, vSF(POP_SIZE));

	for( int i = 0; i < N_ISLANDS; i++ ){
		for( int j = 0; j < POP_SIZE; j++ ){
			pop[i][j].init(DIM, -100.0f, +100.0f);
			aux[i][j].clean_init(DIM, -100.0f, +100.0f);
		}
	}
	v_tuple_3d stats;

	vvi v(N_ISLANDS, vi(POP_SIZE));
	vvi vf(N_ISLANDS, vi(3 * POP_SIZE));

	for( int i = 0; i < N_ISLANDS; i++ ){
		std::iota(v[i].begin(), v[i].end(), 0 );
		std::random_shuffle( v[i].begin(), v[i].end() );
	}

	#if INFO == 1
	for(int i = 0; i < N_ISLANDS; i++){
		printf("\n--------------------------------------------------\n");
		printf("Island #%d\n", i);
		for(int j = 0; j < POP_SIZE; j++ )
			printf("%d ~", v[i][j]);
		printf("\n");
	}
	#endif
	

	calc_fitness_SF(pop, stats);
	//evaluateRosenbrockPop(pop, stats);

	/*
	#if INFO == 1
	for( int i = 0; i < N_ISLANDS; i++ ){
		printf("\n#Island: %d\n", i);
		for( int j = 0; j < POP_SIZE; j++ ){
			printf("\t $ %.20lf\n", pop[i][j].getFitnessValue());
		}
	}
	#endif
	*/

	int N_G = N_GER;
	std::cout << "starting: " << std::endl;
	while(N_G--){

		cpu_index_generator(v, vf);
		
		#if INDEX_GENERATOR == 1
		for(int j = 0; j < N_ISLANDS; j++){
			std::cout << "\n--------------------------------------------------\nIsland #" << j << std::endl;
			for(int i = 0; i < POP_SIZE; i++ )
				std::cout << "threadIdx [" << i << "]:" << vf[j][i] << ", " << vf[j][i + POP_SIZE] << ", " << vf[j][i + 2 * POP_SIZE] << std::endl;
		}
		#endif
		
		differential_evolution(vf, pop, aux);
		subs_better_offsprings(pop, aux, stats);
		//differential_evolution_Rosenbrock(vf, pop, aux);
		//subs_better_offsprings_Rosenbrock(pop, aux, stats);

		//frequencia de migração é um parâmetro, aqui está hard coded com 10;
		/*if( N_G % 10 == 0 ){
			for( int i = 0; i < N_ISLANDS; i++ ){
				std::sort(pop[i].begin(), pop[i].end(), 
					[](const SphereFunction& a, const SphereFunction& b)
					{
						return a.getFitnessValue() > b.getFitnessValue();
					}
				);
				#if MIGRATION_PRINT == 1 
					printf("\n#Island: %d\n", i);
					for( int j = 0; j < POP_SIZE; j++ ){
						printf("\t $ %.5lf\n", pop[i][j].getFitnessValue());
					}
					printf("\n");
				#endif
			}
			//do migration
			for( int i = 0; i < N_ISLANDS; i++ ){
				#if MIGRATION_PRINT == 1
					printf("\n Island #%d will replace %.5lf for %.5lf received from island %d\n", i, pop[i][POP_SIZE - 1].getFitnessValue(),  pop[(i + 1) % N_ISLANDS][0].getFitnessValue(), (i + 1) % N_ISLANDS);
				#endif
				pop[i][POP_SIZE - 1] = pop[(i + 1) % N_ISLANDS][0];
			}
			#if MIGRATION_PRINT == 1
				printf("Population after migration process: \n");
				for( int i = 0; i < N_ISLANDS; i++ ){
					printf("\n#Island: %d\n", i);
					for( int j = 0; j < POP_SIZE; j++ ){
						printf("\t $ %.5lf\n", pop[i][j].getFitnessValue());
					}
					printf("\n");
				}
			#endif
		}*/
	}
	
	std::cout << "end!" << std::endl;
	std::cout << "----------------------------------------------------------" << std::endl;

	int island_idx = 0;
	int pop_idx = 0;
	double fit = 0.0f;

	for( int i = 0; i < N_ISLANDS; i++ ){
		for( int j = 0; j < POP_SIZE; j++ ){
			if( pop[i][j].getFitnessValue() > fit ){
				island_idx = i;
				pop_idx = j;
				fit = pop[i][j].getFitnessValue();
			}
		}
	}/*
	for( int i = 0; i < N_ISLANDS; i++ ){
		for( int j = 0; j < POP_SIZE; j++ ){
			if( pop[i][j].evaluation_value < fit || ( i == 0 && j == 0) ){
				island_idx = i;
				pop_idx = j;
				fit = pop[i][j].evaluation_value;
			}
		}
	}*/

	printf("Best solution finded:\n");
	printf("#island: %d @ind: %d $: %.20lf\n", island_idx, pop_idx, fit);

	printf("Gene: \n");
	for( int i = 0; i < DIM; i++ ){
		printf("   &%d \t %+.20lf\n", i, pop[island_idx][pop_idx].x[i]);
	}

	#if SAVE_STATS == 1
	std::ofstream file;
	file.open("data/output.txt", std::ofstream::out );
	if(file.is_open()){
		file << "G_i F_B F_AVG F_W\n";
		int i = 0;
		for( auto it = stats.begin(); it != stats.end(); it++, i++ ){
			file << i << " " << std::setprecision(20) << std::fixed	<< std::get<0>(*it) << " " << std::get<1>(*it) << " " << std::get<2>(*it) << "\n";
		}
		file.close();
	}else{
		std::cout << "Error opening file\n";
	}
	#endif

	
	return 0;
}
