#include "f_sphere.hpp"
#include "utils.hpp"

SphereFunction::SphereFunction()
{
	/* empty */
}

SphereFunction::SphereFunction( int _D, double _LB, double _UB ):
	D(_D),
	LB(_LB),
	UB(_UB),
	fitness_value(0.000)
{
	x = new double[D];

	max_value = static_cast<double>( UB * UB * D );

	for( int i = 0; i < D; i++ )
		x[i] = random_btw_double_uniform_distribution( LB, UB );

	#if SPHERE_PRINT == 1 
		printf("New SphereFunction defined with %d dimensions\n", D);
		printf("Lower-Bound: %.3lf\n", LB);
		printf("Upper-Bound: %.3lf\n", UB);
		printf("Max value reachable: %.3lf\n", max_value);
	#endif
}

void SphereFunction::copy_data( SphereFunction& b ){
	D  = b.D;
	LB = b.LB;
	UB = b.UB;
	setFitnessValue( b.getFitnessValue() );

	max_value = static_cast<double>( UB * UB * D );

	for( int i = 0; i < D; i++ )
		x[i] = b.x[i];
}

void SphereFunction::init( int _D, double _LB, double _UB )
{
	D  = _D;
	LB = _LB;
	UB = _UB;

	fitness_value = static_cast<double>(0.0000);

	x = new double[D];

	max_value = static_cast<double>( UB * UB * D );

	for( int i = 0; i < D; i++ ){
		x[i] = random_btw_double_uniform_distribution( LB, UB );
	}

	#if INIT_PRINT == 1
		std::cout << "SphereFunction defined with " << D << " dimensions" << std::endl;
		std::cout << "Lower-Bound: " << LB << std::endl;
		std::cout << "Upper-Bound: " << UB << std::endl;
		std::cout << "Max value reachable: " << max_value << std::endl;
		for( int i = 0; i < D; i++ ){
			std::cout << std::setprecision(3) << std::fixed << x[i] << " ";
		}
		std::cout << std::endl;
	#endif
}

void SphereFunction::clean_init( int _D, double _LB, double _UB )
{
	D  = _D;
	LB = _LB;
	UB = _UB;
	fitness_value = 0.0f;

	x = new double[D];
	std::fill_n(x, D, static_cast<double>(0.0f));

	max_value = static_cast<double>( UB * UB * D );
	
	#if INIT_PRINT_C == 1
		std::cout << "SphereFunction defined with " << D << " dimensions" << std::endl;
		std::cout << "Lower-Bound: " << LB << std::endl;
		std::cout << "Upper-Bound: " << UB << std::endl;
		std::cout << "Max value reachable: " << max_value << std::endl;
	#endif
}

SphereFunction::~SphereFunction(void)
{
	//std::cout << "Deleting object" << std::endl;
	if( x != NULL ){
		delete [] x;
		x = NULL;
	}
}

void SphereFunction::print()
{
	std::cout << "SphereFunction defined with " << D << " dimensions" << std::endl;
	std::cout << "Lower-Bound: " << LB << std::endl;
	std::cout << "Upper-Bound: " << UB << std::endl;
	std::cout << "Max value reachable: " << max_value << std::endl;
	
	printf("Valor de Fitness: %.5lf\n", fitness_value);

	for( int i = 0; i < D; i++ ){
		std::cout << std::setprecision(3) << std::fixed << x[i] << " ";
	}
	std::cout << std::endl;
}

double SphereFunction::evaluate()
{
	double sum = 0.0f;
	for(int i = 0; i < D; i++)
	{
		sum += (x[i] * x[i]);
	}
	return sum;
}

double SphereFunction::fitness( double evaluated_value )
{
	double value = 1.0f - static_cast<double>( evaluated_value / max_value );
	setFitnessValue(static_cast<double>(value));
	return value;
}

void SphereFunction::verify_bounds()
{
	for( int i = 0; i < D; i++ )
	{
		if( x[i] > UB )
			x[i] = UB;
		
		if( x[i] < LB )
			x[i] = LB;
	}
}

void SphereFunction::setFitnessValue( double value )
{
	fitness_value = static_cast<double>(value);
}

double SphereFunction::getFitnessValue() const
{
	return static_cast<double>(fitness_value);
}

double SphereFunction::getDimensionValue( int i )
{
	assert(i <= D);
	return x[i];
}

void SphereFunction::do_operations( const SphereFunction& a, const SphereFunction& b, const SphereFunction& c )
{
	double sum = 0.0f;

	for(int i = 0; i < D; i++) {
		/* Pedir ao Parpinelli se essa operação de b.x[i] - c.x[i] deve estar entre módulo. */
		x[i] = a.x[i] + 0.5 * (b.x[i] - c.x[i]);

		if( x[i] > UB )
			x[i] = UB;

		if( x[i] < LB )
			x[i] = LB;

		sum += (x[i] * x[i]);
	}

	setFitnessValue(static_cast<double>(1.0f - static_cast<double>( sum / max_value )));
}
