#include <iostream>
#include <chrono>
#include <functional>
#include <iomanip>
#include <random>
#include <algorithm>

/* C includes */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <assert.h>
#include <string.h>

double random_btw_double_uniform_distribution(const double a, const double b)
{
	std::mt19937 rng; 
    rng.seed(std::random_device{}());
	std::uniform_real_distribution<double> d(-80.000, +80.000);
	double real_rand = d(rng);
	return real_rand;
}

int rand_btw(int a, int b)
{
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> dist(a, b);
	return dist(gen);
}

class SphereFunction {
	public:
		int D;     //dimension
		double LB; //lower-bound
		double UB; //upper-bound
		double *x; //vector with the variables, size D;
		double max_value;

		SphereFunction( );
		
		//SphereFunction(const SphereFunction& b);

		SphereFunction( int, double, double );
		~SphereFunction();

		void operator-=( const SphereFunction& b )
		{
			assert(D  == b.D );
			assert(LB == b.LB);
			assert(UB == b.UB);

			for(int i = 0; i < D; i++)
				x[i] = x[i] - b.x[i];
		}

		SphereFunction& operator=( const SphereFunction& b )
		{
			D  = b.D;
			LB = b.LB;
			UB = b.UB;
			fitness_value = b.fitness_value;

			x = new double[D];

			max_value = UB * UB * D;

			for( int i = 0; i < D; i++ )
				x[i] = b.x[i];

			return *this;
		}

		void print();
		void init( int, double, double );
		void clean_init( int, double, double );

		void setFitnessValue( double value );
		double getFitnessValue() const;

		void copy_data( SphereFunction& b );

	private:
		double fitness_value;		
};

typedef std::vector< SphereFunction > vSF;
typedef std::vector< vSF > vvSF;


SphereFunction::SphereFunction()
{
	/* empty */
}

SphereFunction::SphereFunction( int _D, double _LB, double _UB ):
	D(_D),
	LB(_LB),
	UB(_UB),
	fitness_value(0.000)
{
	x = new double[D];

	max_value = static_cast<double>( UB * UB * D );

	for( int i = 0; i < D; i++ )
		x[i] = random_btw_double_uniform_distribution( LB, UB );

	#if SPHERE_PRINT == 1 
		printf("New SphereFunction defined with %d dimensions\n", D);
		printf("Lower-Bound: %.3lf\n", LB);
		printf("Upper-Bound: %.3lf\n", UB);
		printf("Max value reachable: %.3lf\n", max_value);
	#endif
}

void SphereFunction::copy_data( SphereFunction& b ){
	D  = b.D;
	LB = b.LB;
	UB = b.UB;
	setFitnessValue( b.getFitnessValue() );

	max_value = static_cast<double>( UB * UB * D );

	for( int i = 0; i < D; i++ )
		x[i] = b.x[i];
}

void SphereFunction::init( int _D, double _LB, double _UB )
{
	D  = _D;
	LB = _LB;
	UB = _UB;

	fitness_value = static_cast<double>(0.0000);

	x = new double[D];

	max_value = static_cast<double>( UB * UB * D );

	for( int i = 0; i < D; i++ ){
		x[i] = random_btw_double_uniform_distribution( LB, UB );
	}

	#if SPHERE_PRINT == 1
		cout << "SphereFunction defined with " << D << " dimensions" << endl;
		cout << "Lower-Bound: " << LB << endl;
		cout << "Upper-Bound: " << UB << endl;
		cout << "Max value reachable: " << max_value << endl;
	#endif
}

void SphereFunction::clean_init( int _D, double _LB, double _UB )
{
	D  = _D;
	LB = _LB;
	UB = _UB;
	fitness_value = 0.0f;

	x = new double[D];
	std::fill_n(x, D, static_cast<double>(0.0f));

	max_value = static_cast<double>( UB * UB * D );
	
	#if SPHERE_PRINT == 1
		std::cout << "SphereFunction defined with " << D << " dimensions" << std::endl;
		std::cout << "Lower-Bound: " << LB << std::endl;
		std::cout << "Upper-Bound: " << UB << std::endl;
		std::cout << "Max value reachable: " << max_value << std::endl;
	#endif
}

SphereFunction::~SphereFunction(void)
{
	//std::cout << "Deleting object" << std::endl;
	if( x != NULL ){
		delete [] x;
		x = NULL;
	}
}

void SphereFunction::print()
{
	std::cout << "SphereFunction defined with " << D << " dimensions" << std::endl;
	std::cout << "Lower-Bound: " << LB << std::endl;
	std::cout << "Upper-Bound: " << UB << std::endl;
	std::cout << "Max value reachable: " << max_value << std::endl;
	
	printf("Valor de Fitness: %.5lf\n", fitness_value);

	for( int i = 0; i < D; i++ ){
		std::cout << std::setprecision(3) << std::fixed << x[i] << " ";
	}
	std::cout << std::endl;
}

void SphereFunction::setFitnessValue( double value )
{
	fitness_value = static_cast<double>(value);
}

double SphereFunction::getFitnessValue() const
{
	return static_cast<double>(fitness_value);
}

void calc_fitness_SF(vvSF& pop, int N_ISLANDS, int POP_SIZE){
	int k;
	double sum = 0.0f;
	for( int i = 0; i < N_ISLANDS; i++ ){
		for( int j = 0; j < POP_SIZE; j++ ){
			sum = 0.0f;
		
			for( k = 0; k < pop[i][j].D; k++)
				sum += (pop[i][j].x[k] * pop[i][j].x[k]);

			//std::cout << "[" << j << "]" << "$" << static_cast<double>(1.0f - static_cast<double>( sum / pop[i][j].max_value )) << std::endl;
			
			pop[i][j].setFitnessValue(static_cast<double>(1.0f - static_cast<double>( sum / pop[i][j].max_value )));
		}
	}
}

int main(){	
	std::vector<SphereFunction> TESTE(10);
	for( int i = 0; i < 10; i++){
		TESTE[i].init(50, -80.0f, +80.0f);	
	}
	
	TESTE[0].setFitnessValue(0.3);
	TESTE[1].setFitnessValue(0.1);
	TESTE[2].setFitnessValue(0.6);
	TESTE[3].setFitnessValue(0.2);
	TESTE[4].setFitnessValue(0.9);
	TESTE[5].setFitnessValue(0.4);
	TESTE[6].setFitnessValue(0.8);
	TESTE[7].setFitnessValue(0.5);
	TESTE[8].setFitnessValue(0.7);
	TESTE[9].setFitnessValue(1.0);

	for( int i = 0; i < 10; i++)
		printf("%.3lf ", TESTE[i].getFitnessValue());
	
	printf("\n");
	

	std::sort(TESTE.begin(), TESTE.end(), 
		[](const SphereFunction& a, const SphereFunction& b)
		{
			//printf("comparing %.4lf with %.4lf\n", a.fitness_value, b.fitness_value);
			return a.getFitnessValue() > b.getFitnessValue();
		}
	);
	
	for( int i = 0; i < 10; i++){
		//TESTE[i].print();
		printf("%.3lf ", TESTE[i].getFitnessValue());
	}
	printf("\n");

	printf("===========================================================================\n");

	vvSF T2(3, vSF());

	for( int i = 0; i < 3; i++ ){
		T2[i].resize(10);
		for( int j = 0; j < 10; j++ ){
			T2[i][j].init(50, -80.0f, +80.0f);
		}
	}
	
	calc_fitness_SF(T2, 3, 10);
	
	for( int i = 0; i < 3; i++ ){
		printf("\n#Island: %d\n", i);
		for( int j = 0; j < 10; j++ ){
			printf("\t $ %.3lf\n", T2[i][j].getFitnessValue());
		}
	}
	
	//sort by island	
	for( int i = 0; i < 3; i++ ){
		std::sort(T2[i].begin(), T2[i].end(), 
				[](const SphereFunction& a, const SphereFunction& b)
				{
					//printf("comparing %.4lf with %.4lf\n", a.fitness_value, b.fitness_value);
					return a.getFitnessValue() > b.getFitnessValue();
				}
			);
	}	 

	printf("--------------------------------------------------------------------------\n");
	for( int i = 0; i < 3; i++ ){
		printf("\n#Island: %d\n", i);
		for( int j = 0; j < 10; j++ ){
			printf("\t $ %.3lf\n", T2[i][j].getFitnessValue());
		}
	}

}
