#include <thrust/device_vector.h>
#include <thrust/sort.h>
#include <thrust/execution_policy.h>
#include <iostream>

typedef struct{
	double value;
	int idx;
} SSort;


__global__ void populate(SSort *v){
	int id = threadIdx.x;

	v[id].idx = id;
	v[id].value = 1.318 * id;
}

__global__ void populate2(int *indices, double *values){
	int id = threadIdx.x;

	indices[id] = id;
		values[id] = 1.318 * id;
	}


typedef thrust::device_vector<int> int_dV;
typedef thrust::device_vector<int>::iterator int_dV_it;

typedef thrust::device_vector<double> double_dV;
typedef thrust::device_vector<double>::iterator double_dV_it;

int main(){
  /*
	thrust::device_vector<SSort> vec(20);
	populate<<<1, 20>>>(thrust::raw_pointer_cast(&vec[0]));
	cudaDeviceSynchronize();

	thrust::host_vector<SSort> vect_host = vec;

	thrust::host_vector<SSort>::iterator it = vect_host.begin();

	for(; it < vect_host.end(); it++ ){
		std::cout << (static_cast<SSort>(*it)).idx << " " << (static_cast<SSort>(*it)).value << std::endl;
	}
	*//*

	int_dV indexs(20);
	double_dV values(20);

	populate2<<<1, 20>>>(thrust::raw_pointer_cast(&indexs[0]), thrust::raw_pointer_cast(&values[0]));
	//cudaDeviceSynchronize();

	int_dV_it b = indexs.begin();
	double_dV_it a = values.begin();

	thrust::sort_by_key(thrust::device, a,a + 20 , b, thrust::greater<double>());

	for(int i = 0; i < 20; i++ ){
		std::cout << indexs[i] << " " << values[i] << std::endl;
	}*/

	int indexs2[2][4];
	indexs2[0][0] = indexs2[1][0] = 0;
	indexs2[0][1] = indexs2[1][1] = 1;
	indexs2[0][2] = indexs2[1][2] = 2;
	indexs2[0][3] = indexs2[1][3] = 3;

	double values2[2][4];
	values2[0][0] = values2[1][0] = 0.921;
	values2[0][1] = values2[1][1] = 1.345;
	values2[0][2] = values2[1][2] = 2.2;
	values2[0][3] = values2[1][3] = 1.3;

	thrust::sort_by_key(thrust::host, values2[0], values2[0] + 4 , indexs2[0], thrust::greater<double>());
	thrust::sort_by_key(thrust::host, values2[1], values2[1] + 4 , indexs2[1], thrust::greater<double>());

	for(int i = 0; i < 4; i++ ){
		std::cout << indexs2[0][i] << " " << values2[0][i] << std::endl;
	}
	for(int i = 0; i < 4; i++ ){
		std::cout << indexs2[1][i] << " " << values2[1][i] << std::endl;
	}
	return 0;
}
