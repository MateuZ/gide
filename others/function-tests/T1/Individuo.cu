#include "Individuo.cuh"

__host__ __device__ Individual::Individual()
{
  gene = NULL;
  evaluated_value = 0.0f;
}

__host__ __device__  Individual::~Individual()
{
	/* empty */
}

__host__ void Individual::init( const uint size, const double LB, const double UB )
{
  gene = new double[size];

  std::mt19937 rng;
  rng.seed(std::random_device{}());
  std::uniform_real_distribution<double> d(LB, UB);

  for( int i = 0; i < size; i++ )
	gene[i] = d(rng);
}

__host__ void Individual::clean_init( const uint size )
{
	gene = new double[size];
	std::fill_n(gene, size, static_cast<double>(0.0));
}

__host__ void Individual::print( const uint size )
{
  printf("---------------------------------------------\n");

  for( int i = 0; i < size; i++ )
    printf("[%d]: %+.10lf\n", i, gene[i]);

  printf("---------------------------------------------\n");
}

__host__ __device__  void Individual::copy_data( const Individual& b, const uint size )
{
   memcpy(gene, b.gene, sizeof(double) * size);
   evaluated_value = b.evaluated_value;
}
