#ifndef __INDIVIDUAL__
#define __INDIVIDUAL__

/* C++ includes */

#include <algorithm>

/* C includes */

#include <stdio.h>
#include <string.h>
#include <cuda.h>

class Individual {
  public:
    double *gene;
    double evaluated_value;

    __host__ __device__ Individual();
    __host__ __device__ ~Individual();

    __host__ void init( const uint size, const double LB, const double UB );
    __host__ void clean_init( const uint size );

    __host__ void print( const uint size );

    __host__ __device__ void copy_data( const Individual& b, const uint size );
  private:
    /* empty */
};

#endif
