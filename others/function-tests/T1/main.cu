#include <tuple>
#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <sys/time.h>

/* My libs includes */
#include "Individuo.cuh"

/* CUDA includes */
#include <thrust/sequence.h>
#include <cuda.h>
#include <curand_kernel.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/execution_policy.h>
#include <thrust/fill.h>

typedef std::vector< int > vi;
typedef std::vector< Individual > vS;

typedef thrust::device_vector<int> int_dV;
typedef thrust::device_vector<int>::iterator int_dV_it;

#define CUDA_CALL(x) do { if((x) !=  cudaSuccess) { \
printf ("Error  at %s:%d\n",__FILE__ ,__LINE__); \
return  EXIT_FAILURE ;}}  while (0)

__global__ void teste(double *genes, double *evaluates, int pop_size, int n_dim){
	int index = threadIdx.x;

	printf("%+5.2lf %+5.2lf %+5.2lf\n", genes[(index * n_dim) + 0], genes[(index * n_dim) + 1], genes[(index * n_dim) + 2]);

	
}

__global__ void t2(){
	int index = threadIdx.x + blockDim.x * blockIdx.x;
	printf("%d + %d * %d -> %d\n", threadIdx.x, blockDim.x, blockIdx.x, index);
}

int main(){
	srand(time(NULL));
	int pop_size = 5;
	int n_dim = 3;

	thrust::host_vector<double> hv(pop_size * n_dim);

	std::mt19937 rng;
	rng.seed(std::random_device{}());
	std::uniform_real_distribution<double> d(-5.00, +5.00);
	double temp;
	for( int i = 0; i < (pop_size * n_dim); i++ ){
		temp = d(rng);
		printf("%+3.2lf\n", temp);
		hv[i] = temp;
		
	}
	thrust::device_vector<double> genes;
	genes = hv;

	thrust::device_vector<double> evaluates(pop_size);
	thrust::fill(thrust::device, evaluates.begin(), evaluates.end(), 0.0);

	teste<<<1, pop_size>>>(thrust::raw_pointer_cast(&genes[0]),	thrust::raw_pointer_cast(&evaluates[0]), pop_size, n_dim);
	cudaDeviceSynchronize();
	t2<<< 1, 50 >>>();
	cudaDeviceSynchronize();
	return 0;
}
