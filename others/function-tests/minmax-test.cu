#include <cuda.h>

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/execution_policy.h>
#include <thrust/extrema.h>
#include <thrust/pair.h>

typedef struct{
  int key;
  double value;
} key_value;

__global__ void teste(double *a){
	thrust::pair<double *, double *> result2;
	result2 = thrust::minmax_element(thrust::device, a, a + 10);
	
	uint pos  = result2.first  - &a[0];
	uint pos2 = result2.second - &a[0];
	
	double min = *result2.first;
	double max = *result2.second;
	printf("[%d] %+.2lf [%d] %+.2lf\n", pos, min, pos2, max);
}

int main(){
	thrust::device_vector<double> dV;

	thrust::host_vector<double> hV(10);
	hV[0] = 5.4f;
	hV[1] = 6.4f;
	hV[2] = +59.4f;
	hV[3] = 3.4f;
	hV[4] = 1.4f;
	hV[5] = 21.4f;
	hV[6] = 32.4f;
	hV[7] = -15.4f;
	hV[8] = 21.4f;
	hV[9] = 13.4f;
	
	dV = hV;

	thrust::device_vector<double>::iterator it_dV = dV.begin();
	//thrust::pair<double, double> extrema = thrust::minmax_element(thrust::device, it_dV, it_dV + 10);

	//int data[6] = {1, 0, 2, 2, 1, 3};
	thrust::pair<thrust::host_vector<double>::iterator, thrust::host_vector<double>::iterator> result;
	result = thrust::minmax_element(thrust::host, hV.begin(), hV.begin() + 10);

	unsigned int pos = result.first - hV.begin();
	unsigned int pos2 = result.second - hV.begin();
	double min = *result.first;
	double max = *result.second;
	
	printf("[%d] %+.2lf [%d] %+.2lf\n", pos, min, pos2, max);

	thrust::pair<thrust::device_vector<double>::iterator, thrust::device_vector<double>::iterator> result2;
	result2 = thrust::minmax_element(thrust::device, dV.begin(), dV.begin() + 5);

	pos = result2.first - dV.begin();
	pos2 = result2.second - dV.begin();
	min = *result2.first;
	max = *result2.second;
	printf("[%d] %+.2lf [%d] %+.2lf\n", pos, min, pos2, max);

	teste<<<1, 1>>>(thrust::raw_pointer_cast(&dV[0]));
	cudaDeviceSynchronize();
}
