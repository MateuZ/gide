/* Extern includes */ 

#include "../../source/f_sphere.cuh"

/* C++ includes */

#include <iostream>
#include <chrono>
#include <functional>
#include <iomanip>
#include <random>
#include <algorithm>

/* C includes */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <assert.h>

/* CUDA includes */

#include <thrust/sequence.h>
#include <cuda.h>
#include <curand_kernel.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

double random_btw_normal_distribution(const double a, const double b)
{
	std::mt19937 rng; 
    rng.seed(std::random_device{}());
	std::uniform_real_distribution<double> d(a, b);
	double real_rand = d(rng);
	return real_rand;
}

int main(){
	srand( time(NULL) );
	SphereFunction a;
	a.init(50, -80.0f, +80.0f);
	return 0;
}
