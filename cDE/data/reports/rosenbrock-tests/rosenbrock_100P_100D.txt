 +==============================================================+ 
 |                          CONFIGURAÇÃO                        |
 +==============================================================+ 
 | Número de execuções:      30
 | Tamanho da População:     100
 | Número de Gerações:       10000
 | Número de Dimensões:      100
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.30
 | Função Objetivo:          shifted_rosenbrock
 | Gráfico de Convergência:  False
 +==============================================================+ 
 | Execução: 1  Melhor Global: +479.571(+89.571) Tempo de Execução (s): 8.262
 | Execução: 2  Melhor Global: +479.757(+89.757) Tempo de Execução (s): 8.362
 | Execução: 3  Melhor Global: +480.434(+90.434) Tempo de Execução (s): 8.266
 | Execução: 4  Melhor Global: +479.975(+89.975) Tempo de Execução (s): 8.173
 | Execução: 5  Melhor Global: +479.929(+89.929) Tempo de Execução (s): 8.262
 | Execução: 6  Melhor Global: +479.844(+89.844) Tempo de Execução (s): 8.271
 | Execução: 7  Melhor Global: +479.935(+89.935) Tempo de Execução (s): 8.428
 | Execução: 8  Melhor Global: +479.687(+89.687) Tempo de Execução (s): 8.326
 | Execução: 9  Melhor Global: +479.750(+89.750) Tempo de Execução (s): 8.312
 | Execução: 10 Melhor Global: +479.749(+89.749) Tempo de Execução (s): 8.298
 | Execução: 11 Melhor Global: +480.210(+90.210) Tempo de Execução (s): 8.210
 | Execução: 12 Melhor Global: +479.948(+89.948) Tempo de Execução (s): 8.399
 | Execução: 13 Melhor Global: +479.667(+89.667) Tempo de Execução (s): 8.346
 | Execução: 14 Melhor Global: +479.925(+89.925) Tempo de Execução (s): 8.186
 | Execução: 15 Melhor Global: +479.533(+89.533) Tempo de Execução (s): 8.208
 | Execução: 16 Melhor Global: +479.859(+89.859) Tempo de Execução (s): 8.282
 | Execução: 17 Melhor Global: +479.542(+89.542) Tempo de Execução (s): 8.446
 | Execução: 18 Melhor Global: +479.816(+89.816) Tempo de Execução (s): 8.400
 | Execução: 19 Melhor Global: +480.022(+90.022) Tempo de Execução (s): 8.139
 | Execução: 20 Melhor Global: +479.974(+89.974) Tempo de Execução (s): 8.190
 | Execução: 21 Melhor Global: +479.480(+89.480) Tempo de Execução (s): 8.224
 | Execução: 22 Melhor Global: +480.007(+90.007) Tempo de Execução (s): 8.363
 | Execução: 23 Melhor Global: +480.214(+90.214) Tempo de Execução (s): 8.237
 | Execução: 24 Melhor Global: +479.675(+89.675) Tempo de Execução (s): 8.233
 | Execução: 25 Melhor Global: +479.728(+89.728) Tempo de Execução (s): 8.284
 | Execução: 26 Melhor Global: +480.003(+90.003) Tempo de Execução (s): 8.534
 | Execução: 27 Melhor Global: +481.442(+91.442) Tempo de Execução (s): 8.507
 | Execução: 28 Melhor Global: +479.847(+89.847) Tempo de Execução (s): 8.572
 | Execução: 29 Melhor Global: +480.175(+90.175) Tempo de Execução (s): 8.401
 | Execução: 30 Melhor Global: +479.828(+89.828) Tempo de Execução (s): 8.327
 +==============================================================+ 
 |                   RESULTADOS DOS EXPERIMENTOS                | 
 +==============================================================+ 
 | Função Objetivo:
 | 	 Média:                 +479.917
 | 	 Devio-Padrão:          +0.355
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +89.917
 | 	 Desvio-Padrão:         +0.355
 | Tempo de Execução: 
 | 	 Média:                 +8.315
 | 	 Devio-Padrão:          +0.108
 +==============================================================+
