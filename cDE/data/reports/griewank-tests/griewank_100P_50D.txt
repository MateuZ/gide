 +==============================================================+ 
 |                          CONFIGURAÇÃO                        |
 +==============================================================+ 
 | Número de execuções:      30
 | Tamanho da População:     100
 | Número de Gerações:       5000
 | Número de Dimensões:      50
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.30
 | Função Objetivo:          shifted_griewank
 | Gráfico de Convergência:  False
 +==============================================================+ 
 | Execução: 1  Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.258
 | Execução: 2  Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.252
 | Execução: 3  Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.251
 | Execução: 4  Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.262
 | Execução: 5  Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.255
 | Execução: 6  Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.249
 | Execução: 7  Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.251
 | Execução: 8  Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.260
 | Execução: 9  Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.251
 | Execução: 10 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.246
 | Execução: 11 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.245
 | Execução: 12 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.248
 | Execução: 13 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.239
 | Execução: 14 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.247
 | Execução: 15 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.246
 | Execução: 16 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.237
 | Execução: 17 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.239
 | Execução: 18 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.238
 | Execução: 19 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.260
 | Execução: 20 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.247
 | Execução: 21 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.236
 | Execução: 22 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.245
 | Execução: 23 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.241
 | Execução: 24 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.245
 | Execução: 25 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.242
 | Execução: 26 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.248
 | Execução: 27 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.243
 | Execução: 28 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.250
 | Execução: 29 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.285
 | Execução: 30 Melhor Global: -180.000(+0.000) Tempo de Execução (s): 4.265
 +==============================================================+ 
 |                   RESULTADOS DOS EXPERIMENTOS                | 
 +==============================================================+ 
 | Função Objetivo:
 | 	 Média:                 -180.000
 | 	 Devio-Padrão:          +0.000
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +0.000
 | 	 Desvio-Padrão:         +0.000
 | Tempo de Execução: 
 | 	 Média:                 +4.249
 | 	 Devio-Padrão:          +0.010
 +==============================================================+ 
