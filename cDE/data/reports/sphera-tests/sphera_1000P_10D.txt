 +==============================================================+ 
 |                          CONFIGURAÇÃO                        |
 +==============================================================+ 
 | Número de execuções:      30
 | Tamanho da População:     1000
 | Número de Gerações:       100
 | Número de Dimensões:      10
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.30
 | Função Objetivo:          shifted_sphere
 | Gráfico de Convergência:  False
 +==============================================================+ 
 | Execução: 1  Melhor Global: -449.791(+0.209) Tempo de Execução (s): 0.673
 | Execução: 2  Melhor Global: -449.853(+0.147) Tempo de Execução (s): 0.674
 | Execução: 3  Melhor Global: -449.839(+0.161) Tempo de Execução (s): 0.671
 | Execução: 4  Melhor Global: -449.922(+0.078) Tempo de Execução (s): 0.671
 | Execução: 5  Melhor Global: -449.850(+0.150) Tempo de Execução (s): 0.673
 | Execução: 6  Melhor Global: -449.847(+0.153) Tempo de Execução (s): 0.672
 | Execução: 7  Melhor Global: -449.786(+0.214) Tempo de Execução (s): 0.673
 | Execução: 8  Melhor Global: -449.884(+0.116) Tempo de Execução (s): 0.671
 | Execução: 9  Melhor Global: -449.798(+0.202) Tempo de Execução (s): 0.680
 | Execução: 10 Melhor Global: -449.769(+0.231) Tempo de Execução (s): 0.682
 | Execução: 11 Melhor Global: -449.881(+0.119) Tempo de Execução (s): 0.677
 | Execução: 12 Melhor Global: -449.849(+0.151) Tempo de Execução (s): 0.680
 | Execução: 13 Melhor Global: -449.883(+0.117) Tempo de Execução (s): 0.689
 | Execução: 14 Melhor Global: -449.834(+0.166) Tempo de Execução (s): 0.674
 | Execução: 15 Melhor Global: -449.834(+0.166) Tempo de Execução (s): 0.672
 | Execução: 16 Melhor Global: -449.793(+0.207) Tempo de Execução (s): 0.671
 | Execução: 17 Melhor Global: -449.820(+0.180) Tempo de Execução (s): 0.682
 | Execução: 18 Melhor Global: -449.809(+0.191) Tempo de Execução (s): 0.701
 | Execução: 19 Melhor Global: -449.804(+0.196) Tempo de Execução (s): 0.683
 | Execução: 20 Melhor Global: -449.881(+0.119) Tempo de Execução (s): 0.704
 | Execução: 21 Melhor Global: -449.820(+0.180) Tempo de Execução (s): 0.674
 | Execução: 22 Melhor Global: -449.872(+0.128) Tempo de Execução (s): 0.675
 | Execução: 23 Melhor Global: -449.865(+0.135) Tempo de Execução (s): 0.706
 | Execução: 24 Melhor Global: -449.849(+0.151) Tempo de Execução (s): 0.702
 | Execução: 25 Melhor Global: -449.856(+0.144) Tempo de Execução (s): 0.678
 | Execução: 26 Melhor Global: -449.881(+0.119) Tempo de Execução (s): 0.672
 | Execução: 27 Melhor Global: -449.890(+0.110) Tempo de Execução (s): 0.672
 | Execução: 28 Melhor Global: -449.790(+0.210) Tempo de Execução (s): 0.693
 | Execução: 29 Melhor Global: -449.823(+0.177) Tempo de Execução (s): 0.679
 | Execução: 30 Melhor Global: -449.795(+0.205) Tempo de Execução (s): 0.685
 +==============================================================+ 
 |                   RESULTADOS DOS EXPERIMENTOS                | 
 +==============================================================+ 
 | Função Objetivo:
 | 	 Média:                 -449.839
 | 	 Devio-Padrão:          +0.038
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +0.161
 | 	 Desvio-Padrão:         +0.038
 | Tempo de Execução: 
 | 	 Média:                 +0.680
 | 	 Devio-Padrão:          +0.011
 +==============================================================+ 

