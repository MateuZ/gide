#ifndef __DE__
#define __DE__

/* C++ includes */
#include <tuple>
#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <sys/time.h>

/* My libs includes */
#include "Individuo.hpp"
#include "ShiftedGriewank.hpp"
#include "ShiftedSphere.hpp"
#include "ShiftedRastrigin.hpp"
#include "ShiftedRosenbrock.hpp"

typedef std::vector< int > vi;
typedef std::vector< Individual > vS;
typedef std::tuple<double, double, double> tuple_3d;
typedef std::vector< tuple_3d > v_tuple_3d;

double stime();

int rand_btw(int a, int b);

void cpu_index_generator( vi& v, vi& vf, uint pop_size);

template <class T>
void differential_evolution(
  vi& vf,
  vS& pop, vS& aux,
  uint pop_size, uint n_dim,
  double F, double CR,
  T& FuncObj
);

void subs_better_offspring(vS& pop, vS& aux, uint pop_size, uint n_dim, v_tuple_3d& stats);

void save_convergence_file(v_tuple_3d& stats);

std::tuple<double, double, double> DE_execute(
  uint pop_size, uint n_ger, uint n_dim,
  double mf, double cr,
  std::string FuncObj, std::string output_file,
  bool conv_plot
);

#endif
