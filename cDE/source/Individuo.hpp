#ifndef __INDIVIDUAL__
#define __INDIVIDUAL__

/* C++ includes */

#include <algorithm>

/* C includes */

#include <stdio.h>
#include <string.h>

class Individual {
  public:
    double *gene;
    double evaluated_value;

    Individual();
    ~Individual();

    void init( const uint size, const double LB, const double UB );
    void clean_init( const uint size );

    void print( const uint size );

    void copy_data( const Individual& b, const uint size );
  private:
    /* empty */
};

#endif
