#include "DE.hpp"

void save_convergence_file(v_tuple_3d& stats, std::string file_name){
  std::ofstream file;
  file_name = "data/" + file_name;
  file.open(file_name.c_str(), std::ofstream::out );
  if(file.is_open()){
    file << "G_i F_B F_AVG F_W\n";
    int i = 0;
    for( auto it = stats.begin(); it != stats.end(); it++, i++ ){
      file << i << " " << std::setprecision(20) << std::fixed	<< std::get<0>(*it) << " " << std::get<1>(*it) << " " << std::get<2>(*it) << "\n";
    }
    file.close();
  }else{
    std::cout << "Error opening file to safe stats\n";
  }
}

double stime(){
    struct timeval tv;
    struct timezone tz;
    gettimeofday(&tv, &tz);
    double mlsec = 1000.0 * ((double)tv.tv_sec + (double)tv.tv_usec/1000000.0);
    return mlsec/1000.0;
}

int rand_btw(int a, int b){
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> dist(a, b);
	return dist(gen);
}

void cpu_index_generator( vi& v, vi& vf, uint pop_size){
  int n1, n2, n3;
  for( uint j = 0; j < pop_size; j++ ){
    n1 = rand_btw(0, pop_size-1);
    //std::cout << "n1: " << n1 << std::endl;

    vf[j] = v[n1];

    if( v[n1] == j ){
    	n1 = (n1 + 1) % pop_size;
    	vf[j] = v[n1];
    }

    n2 = rand_btw(1, static_cast<int>(pop_size/3));

    vf[j + pop_size] = v[ (n1 + n2) % pop_size ];

    if ( vf[j + pop_size] == j){
    	n2 = (n2 + 1) % pop_size;
    	vf[j + pop_size] = v[ (n1 + n2) % pop_size ];
    }

    n3 = rand_btw(1, static_cast<int>(pop_size/3));;

    vf[j + 2 * pop_size] = v[ (n1 + n2 + n3) % pop_size ];

    if( vf[j + 2 * pop_size] == j ){
    	n3 = ( n3 + 1 ) % pop_size;
    	vf[j + 2 * pop_size] = v[ (n1 + n2 + n3) % pop_size ];
    }
  }
}

template <class T>
void differential_evolution(
  vi& vf,
  vS& pop, vS& aux,
  uint pop_size, uint n_dim,
  double F, double CR,
  T& FuncObj
){
  std::mt19937 rng;
  rng.seed(std::random_device{}());
  std::uniform_real_distribution<double> d(0.00, 1.00);

	int n1, n2, n3, rnbr;
	for( int j = 0; j < pop_size; j++ ){
		rnbr = rand_btw(0, n_dim-1);
		n1 = vf[j];
		n2 = vf[j + pop_size];
		n3 = vf[j + 2 * pop_size];

		for( int k = 0; k < n_dim; k++ ){
			if( (d(rng) < CR) or (k == (n_dim-1)) ){
				aux[j].gene[rnbr] = pop[n3].gene[rnbr] + F * (pop[n1].gene[rnbr] - pop[n2].gene[rnbr]);

				if( aux[j].gene[rnbr] > FuncObj.getUpperBound() )
					aux[j].gene[rnbr] = FuncObj.getUpperBound();

				if( aux[j].gene[rnbr] < FuncObj.getLowerBound() )
					aux[j].gene[rnbr] = FuncObj.getLowerBound();

			}else{
				//receive an equal dimension;
				aux[j].gene[rnbr] = pop[j].gene[rnbr];
			}
      rnbr = (rnbr+1)%n_dim;;
		}
    aux[j].evaluated_value = FuncObj.evaluate(aux[j].gene, n_dim);
	}
}

void subs_better_offspring(vS& pop, vS& aux, uint pop_size, uint n_dim, v_tuple_3d& stats){
  double best_f, worst_f, avg_f =0.0f, temp;
	for( int j = 0; j < pop_size; j++ ){
		if(aux[j].evaluated_value <= pop[j].evaluated_value){
			pop[j].copy_data( aux[j], n_dim );
    }
    temp = pop[j].evaluated_value;
    if( temp < best_f || j == 0 )
      best_f = temp;

    if( temp > worst_f || j == 0)
      worst_f = temp;

    avg_f += temp / pop_size;
  }
  stats.push_back(std::make_tuple(best_f, avg_f, worst_f));
}

std::tuple<double, double, double> DE_execute(
  uint pop_size, uint n_ger, uint n_dim,
  double mf, double cr,
  std::string FuncObj, std::string output_file,
  bool conv_plot
){
  vi v(pop_size);
  vi vf(3 * pop_size);
  v_tuple_3d stats;

  std::iota(v.begin(), v.end(), 0 );
  std::random_shuffle( v.begin(), v.end() );

  vS pop(pop_size);
  vS aux(pop_size);

  double tini, tend, best, EFV;

  if( FuncObj == "shifted_griewank"){
    tini = stime();

    ShiftedGriewank sg;
    for(int i = 0; i < pop_size; i++){
      pop[i].init(n_dim, sg.getLowerBound(), sg.getUpperBound() );
      aux[i].clean_init(n_dim);
      pop[i].evaluated_value = sg.evaluate(pop[i].gene, n_dim);
    }
    for(int i = 0; i < n_ger; i++){
      cpu_index_generator(v, vf, pop_size);
      differential_evolution(vf, pop, aux, pop_size, n_dim, mf, cr, sg);
      subs_better_offspring(pop, aux, pop_size, n_dim, stats);
    }
    tend = stime();
    for(int i = 0; i < pop_size; i++){
      if( pop[i].evaluated_value < best || i == 0){
        best = pop[i].evaluated_value;
      }
    }
    EFV = fabs(fabs(best) - 180.00f);
  }else if( FuncObj == "shifted_sphere" ){
    tini = stime();

    ShiftedSphere ss;
    for(int i = 0; i < pop_size; i++){
      pop[i].init(n_dim, ss.getLowerBound(), ss.getUpperBound() );
      aux[i].clean_init(n_dim);
      pop[i].evaluated_value = ss.evaluate(pop[i].gene, n_dim);
    }
    for(int i = 0; i < n_ger; i++){
      cpu_index_generator(v, vf, pop_size);
      differential_evolution(vf, pop, aux, pop_size, n_dim, mf, cr, ss);
      subs_better_offspring(pop, aux, pop_size, n_dim, stats);
    }
    tend = stime();
    for(int i = 0; i < pop_size; i++){
      if( pop[i].evaluated_value < best || i == 0){
        best = pop[i].evaluated_value;
      }
    }
    EFV = fabs(fabs(best) - 450.00f);
  }
  else if( FuncObj == "shifted_rastrigin" )
  {
    tini = stime();

    ShiftedRastrigin sr;
    for(int i = 0; i < pop_size; i++){
      pop[i].init(n_dim, sr.getLowerBound(), sr.getUpperBound() );
      aux[i].clean_init(n_dim);
      pop[i].evaluated_value = sr.evaluate(pop[i].gene, n_dim);
    }
    for(int i = 0; i < n_ger; i++){
      cpu_index_generator(v, vf, pop_size);
      differential_evolution(vf, pop, aux, pop_size, n_dim, mf, cr, sr);
      subs_better_offspring(pop, aux, pop_size, n_dim, stats);
    }

    tend = stime();
    for(int i = 0; i < pop_size; i++){
      if( pop[i].evaluated_value < best || i == 0){
        best = pop[i].evaluated_value;
      }
    }
    EFV = fabs(fabs(best) - 330.00f);
  }
  else if( FuncObj == "shifted_rosenbrock"){
    tini = stime();

    ShiftedRosenbrock sr;
    for(int i = 0; i < pop_size; i++){
      pop[i].init(n_dim, sr.getLowerBound(), sr.getUpperBound() );
      aux[i].clean_init(n_dim);
      pop[i].evaluated_value = sr.evaluate(pop[i].gene, n_dim);
    }
    for(int i = 0; i < n_ger; i++){
      cpu_index_generator(v, vf, pop_size);
      differential_evolution(vf, pop, aux, pop_size, n_dim, mf, cr, sr);
      subs_better_offspring(pop, aux, pop_size, n_dim, stats);
    }
    tend = stime();
    for(int i = 0; i < pop_size; i++){
      if( pop[i].evaluated_value < best || i == 0){
        best = pop[i].evaluated_value;
      }
    }
    EFV = fabs(fabs(best) - 390.00f);
  }
  /*CODE END*/
  if(conv_plot){
    save_convergence_file(stats, output_file);
  }
  return std::make_tuple(best, tend-tini, EFV );
}
