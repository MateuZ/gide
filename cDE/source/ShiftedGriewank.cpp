#include "ShiftedGriewank.hpp"

ShiftedGriewank::ShiftedGriewank(){
  m_lowerBound = -600;
  m_upperBound =  600;
  m_fBias = -180.0;
}

ShiftedGriewank::~ShiftedGriewank(){
  /* empty */
}

double ShiftedGriewank::getOptimalPoint(int size){
  return -180.00f;
};

double ShiftedGriewank::evaluate(double individual[], int size){
	double z = 0;
	double top1 = 0;
	double top2 = 0;
	unsigned short int i;

	top1 = 0.0;
	top2 = 1.0;
	for(i=0;i<size;i++){
		z = individual[i] - griewank[i];
		top1 = top1 + static_cast<double>(( pow(z,2) / 4000 ));
		top2 = static_cast<double>(top2 * ( cos(z/sqrt(i+1))));
	}
	top1 =  (top1 - top2 + 1.0 + m_fBias);
	if(top1 - getOptimalPoint(size) <= 10e-8)	{
		top1 = getOptimalPoint(size);
	}
	return top1;
}

std::string ShiftedGriewank::getName(){
	return "ShiftedGriewank";
}

double ShiftedGriewank::verifyBounds(double d){
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double ShiftedGriewank::getLowerBound(){
	return m_lowerBound;
}

double ShiftedGriewank::getUpperBound(){
	return m_upperBound;
}
