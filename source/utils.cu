#include "utils.cuh"

double random_btw_normal_distribution(const double a, const double b)
{
	std::mt19937 rng; 
    rng.seed(std::random_device{}());
	std::uniform_real_distribution<double> d(a, b);
	double real_rand = d(rng);
	return real_rand;
}