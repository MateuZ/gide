#ifndef __F_SPHERE__
#define __F_SPHERE__

#include <assert.h>

#define SPHERE_PRINT 0

class SphereFunction {
	public:
		int D;     //dimension
		double LB; //lower-bound
		double UB; //upper-bound
		double *x; //vector with the variables, size D;
		double max_value;

		__device__ __host__ SphereFunction( );
		SphereFunction( int, double, double );
		__device__ __host__ ~SphereFunction();

		__device__ __host__ void operator-=( const SphereFunction& b )
		{
			assert(D  == b.D );
			assert(LB == b.LB);
			assert(UB == b.UB);

			for(int i = 0; i < D; i++)
				x[i] = x[i] - b.x[i];
		}

		__device__ __host__ void operator=( const SphereFunction& b )
		{
			D  = b.D;
			LB = b.LB;
			UB = b.UB;

			x = new double[D];

			max_value = UB * UB * D;

			for( int i = 0; i < D; i++ )
				x[i] = b.x[i];
		}

		__device__ __host__ void operator*=( const double& FM )
		{
			for( int i = 0; i < D; i++ )
				x[i] *= FM;
		}

		__device__ __host__ void operator+=( const SphereFunction& b )
		{
			assert(D  == b.D );
			assert(LB == b.LB);
			assert(UB == b.UB);

			for(int i = 0; i < D; i++)
				x[i] = x[i] + b.x[i];
		}

		double evaluate();
		double fitness( double );

		
		void print();
		void init( int, double, double );
		void clean_init( int, double, double );

		__device__ __host__ void verify_bounds();
		__device__ __host__ void setFitnessValue( const double& value );
		__device__ __host__ double getFitnessValue();
		__device__ __host__ double getDimensionValue( const int& i );
		__device__ __host__ void do_operations( const SphereFunction& a, const SphereFunction& b, const SphereFunction& c );
		__device__ __host__ void copy_data( SphereFunction& b );

	private:
		double fitness_value;
};

#endif
