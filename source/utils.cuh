#ifndef __UTIL__
#define __UTIL__

/* C++ includes */

#include <iostream>
#include <chrono>
#include <functional>
#include <iomanip>
#include <random>
#include <algorithm>

/* C includes */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <assert.h>

/* CUDA includes */

#include <thrust/sequence.h>
#include <cuda.h>
#include <curand_kernel.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

#define CUDA_CALL(x) do { if((x) !=  cudaSuccess) { \
printf ("Error  at %s:%d\n",__FILE__ ,__LINE__); \
return  EXIT_FAILURE ;}}  while (0)

double random_btw_normal_distribution(const double a, const double b);

#endif
