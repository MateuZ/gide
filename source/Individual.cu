#include "Individual.cuh"
#include "utils.cuh"

__device__ __host__ Individual::Individual()
{
	/* empty */
}

__device__ __host__ Individual::~Individual()
{
	/* empty */
}

void Individual::init( const uint size, const double LB, const double UB )
{
	cudaMallocManaged(&x, size * sizeof(double));

	for( int i = 0; i < size; i++ ){
		x[i] = random_btw_normal_distribution( LB, UB );
    #if INIT_PRINT == 1
        printf("[%d]: %+.20lf\n", i, x[i]);
    #endif
	}
}

void Individual::clean_init( const uint size )
{
	cudaMallocManaged(&x, size * sizeof(double));
	std::fill_n(x, size, static_cast<double>(0.0));
}

void Individual::print( const uint size )
{
  printf("---------------------------------------------\n");

  for( int i = 0; i < size; i++ )
    printf("[%d]: %+.20lf\n", i, x[i]);

  printf("---------------------------------------------\n");
}

__device__ __host__ void Individual::copy_data( const Individual& b, const uint size )
{
  memcpy(x, b.x, sizeof(double) * size);
	evaluation_value = b.evaluation_value;
}
