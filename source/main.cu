#include "utils.cuh"
#include "f_sphere.cuh"
#include "Individual.cuh"

#define INFO 0
#define INIT_PRINT 0
#define KERNEL_PRINT 0
#define DEBUG_CALLS 0

#define POP_SIZE   100
#define N_GER     5000 
#define N_ISLANDS    1
#define F          0.5

#define DIM					50
#define UB					80.0
#define LB					-80.0
#define CR					0.9

__global__ void device_calc_fitness_SF(Individual **pop){
	int i = threadIdx.x;
	int j = blockIdx.x;
	int k;
	double sum = 0.0f;
	for( k = 0; k < DIM; k++)
		sum += (pop[j][i].x[k] * pop[j][i].x[k]);

	pop[j][i].evaluation_value = sum;
}

__global__ void differential_evolution(
	int **vf,
	Individual **pop,
	Individual **aux
){
	int isl_id = blockIdx.x;
	int ind_id = threadIdx.x;
	int n1, n2, n3;

	curandState s;
	curand_init((ind_id + (isl_id * blockDim.x) + 1) * clock(), 0, 0, &s);

	n1 = vf[isl_id][ind_id];
	n2 = vf[isl_id][ind_id + POP_SIZE];
	n3 = vf[isl_id][ind_id + 2*POP_SIZE];

	int rnbr = curand(&s)%DIM;
	//printf("[%d, %d] rnbr: %d and %.2lf\n", isl_id, ind_id, rnbr, );
	//crossover!
	for( int k = 0; k < DIM; k++ ){
		if( (curand_uniform(&s) <= CR) or (k == rnbr) ){
			//receive a mutation dimension;
			aux[isl_id][ind_id].x[k] = pop[isl_id][n1].x[k] + F * (pop[isl_id][n2].x[k] - pop[isl_id][n3].x[k]);

			if( aux[isl_id][ind_id].x[k] > UB )
				aux[isl_id][ind_id].x[k] = UB;

			if( aux[isl_id][ind_id].x[k] < LB )
				aux[isl_id][ind_id].x[k] = LB;

		}else{
			//receive an equal dimension;
			aux[isl_id][ind_id].x[k] = pop[isl_id][isl_id].x[k];
		}
	}
}

__global__ void subs_better_offsprings( Individual **pop, Individual **aux ){
    int isl_id = blockIdx.x;
    int ind_id = threadIdx.x;

    if(aux[isl_id][ind_id].evaluation_value < pop[isl_id][ind_id].evaluation_value){
        #if KERNEL_PRINT == 1
            printf("Island: [#%d] Ind: [%d] subs -> $%.10lf x $%.10lf\n", isl_id, ind_id, aux[isl_id][ind_id].getFitnessValue(), pop[isl_id][ind_id].getFitnessValue());
        #endif
        pop[isl_id][ind_id].copy_data( aux[isl_id][ind_id], DIM );
    }
}

__global__ void index_generator(int **v, int **vf){
	int i = threadIdx.x;
	int j = blockIdx.x;

	curandState s;
	int n1, n2, n3;
	curand_init((i + 1) * clock(), 0, 0, &s);

	n1 = curand(&s) % POP_SIZE;

	vf[j][i] = v[j][n1];
	if( v[j][n1] == i ){
        n1 = (n1 + 1) % POP_SIZE;
		vf[j][i] = v[j][n1];
	}

    n2 = (curand(&s) % (int)POP_SIZE/3) + 1;

	vf[j][i + POP_SIZE] = v[j][(n1 + n2) % POP_SIZE];
	if( vf[j][i + POP_SIZE] == i ){
		n2 = (n2 + 1) % POP_SIZE;
		vf[j][i + POP_SIZE] = v[j][(n1 + n2) % POP_SIZE];
	}

	n3 = (curand(&s) % (int)POP_SIZE/3) + 1;

	vf[j][i + 2 * POP_SIZE] = v[j][(n1 + n2 + n3) % POP_SIZE];
	if( vf[j][i + 2 * POP_SIZE] == i ){
		n3 = (n3 + 1) % POP_SIZE;
		vf[j][i + 2 * POP_SIZE] = v[j][(n1 + n2 + n3) % POP_SIZE];
	}
}

int main(void)
{
	Individual **pop;
	Individual **aux;

	cudaMallocManaged( &pop, N_ISLANDS * sizeof(Individual) );
	cudaMallocManaged( &aux, N_ISLANDS * sizeof(Individual) );

	for( int i = 0; i < N_ISLANDS; i++ ){
		cudaMallocManaged( &pop[i], POP_SIZE * sizeof(Individual) );
		cudaMallocManaged( &aux[i], POP_SIZE * sizeof(Individual) );
		for( int j = 0; j < POP_SIZE; j++ ){
			pop[i][j].init(DIM, -80.0f, +80.0f);
			aux[i][j].clean_init(DIM);
		}
	}

	int **v, **h_v;
	int **vf, **h_vf;

	int *sequence_vector;
  sequence_vector = (int *) malloc ( POP_SIZE * sizeof(int) );
  thrust::sequence(sequence_vector, sequence_vector + POP_SIZE );

  h_v = (int **) malloc ( N_ISLANDS * sizeof(int *) );
  CUDA_CALL(cudaMalloc((void***)&v, N_ISLANDS * sizeof(int *) ));

  h_vf = (int **) malloc ( N_ISLANDS * sizeof(int *) );
  CUDA_CALL(cudaMalloc((void***)&vf, N_ISLANDS * sizeof(int *) ));

  for( int i = 0; i < N_ISLANDS; i++ ) {
      CUDA_CALL(cudaMalloc((void**)&(h_v[i]), POP_SIZE * sizeof(int) ));
      cudaMemcpy(h_v[i], sequence_vector, POP_SIZE * sizeof(int), cudaMemcpyHostToDevice);

      CUDA_CALL(cudaMalloc((void**)&(h_vf[i]), POP_SIZE * 3 * sizeof(int) ));
      CUDA_CALL(cudaMemset(h_vf[i], 0, POP_SIZE * 3 * sizeof(int)));
  }
  CUDA_CALL(cudaMemcpy(v, h_v, N_ISLANDS * sizeof(int *), cudaMemcpyHostToDevice ));
  CUDA_CALL(cudaMemcpy(vf, h_vf, N_ISLANDS * sizeof(int *), cudaMemcpyHostToDevice ));

	int N_G = N_GER;

  std::cout << "starting: " << std::endl;

	/*
		EVALUATE THE POP INDV;
	*/
	device_calc_fitness_SF<<<N_ISLANDS, POP_SIZE>>>(pop);
  #if DEBUG_CALLS == 1
      CUDA_CALL(cudaDeviceSynchronize());
  #endif
  while( N_G-- ){
		index_generator<<<N_ISLANDS, POP_SIZE>>>(v, vf);

		#if INFO == 1
			for(int j = 0; j < N_ISLANDS; j++){
				cout << "\n--------------------------------------------------\nIsland #" << j << endl;
				for(int i = 0; i < POP_SIZE; i++ )
					cout << "threadIdx [" << i << "]:" << vf[j][i] << ", " << vf[j][i + POP_SIZE] << ", " << vf[j][i + 2 * POP_SIZE] << endl;
			}
    #endif

		differential_evolution<<<N_ISLANDS, POP_SIZE>>>(vf, pop, aux);
    #if DEBUG_CALLS == 1
    	CUDA_CALL(cudaDeviceSynchronize());
    #endif

		device_calc_fitness_SF<<<N_ISLANDS, POP_SIZE>>>(aux);

    subs_better_offsprings<<<N_ISLANDS, POP_SIZE>>>(pop, aux);
		#if DEBUG_CALLS == 1
    	CUDA_CALL(cudaDeviceSynchronize());
		#endif

		//migration
		

  }
  CUDA_CALL(cudaDeviceSynchronize());

  std::cout << "end!" << std::endl;
	std::cout << "----------------------------------------------------------" << std::endl;

	printf("Best solution finded:\n");
	printf("#island: %d @ind: %d $: %.20lf\n", 0, 0, 0.0);

	return 0;
}
