#ifndef __INDIVIDUAL__
#define __INDIVIDUAL__

class Individual {
	public:
    //gene
		double *x;
    double evaluation_value;

		__device__ __host__ Individual( /*empty*/ );

		__device__ __host__ ~Individual( /*empty*/ );

    void init( const uint size, const double LB, const double UB );
    void clean_init( const uint size );

    void print( const uint size );
    __device__ __host__ void copy_data( const Individual& b, const uint size );

	private:
    /* empty */
};

#endif
