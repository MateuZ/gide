#include "f_sphere.cuh"
#include "utils.cuh"

__device__ __host__ SphereFunction::SphereFunction()
{
	/* empty */
}

SphereFunction::SphereFunction( int _D, double _LB, double _UB ):
	D(_D),
	LB(_LB),
	UB(_UB),
	fitness_value(0)
{
	x = new double[D];

	max_value = static_cast<double>( UB * UB * D );

	for( int i = 0; i < D; i++ )
		x[i] = random_btw_normal_distribution( LB, UB );

	#if SPHERE_PRINT == 1 
		printf("New SphereFunction defined with %d dimensions\n", D);
		printf("Lower-Bound: %.3lf\n", LB);
		printf("Upper-Bound: %.3lf\n", UB);
		printf("Max value reachable: %.3lf\n", max_value);
	#endif
}

__device__ __host__ void SphereFunction::copy_data( SphereFunction& b )
{
	setFitnessValue( b.getFitnessValue() );
	memcpy(x, b.x, sizeof(double) * D);
}

void SphereFunction::init( int _D, double _LB, double _UB )
{
	D  = _D;
	LB = _LB;
	UB = _UB;
	fitness_value = 0.0f;

	cudaMallocManaged(&x, D * sizeof(double));

	max_value = static_cast<double>( UB * UB * D );

	for( int i = 0; i < D; i++ ){
		x[i] = random_btw_normal_distribution( LB, UB );
	}

	#if SPHERE_PRINT == 1
		cout << "SphereFunction defined with " << D << " dimensions" << endl;
		cout << "Lower-Bound: " << LB << endl;
		cout << "Upper-Bound: " << UB << endl;
		cout << "Max value reachable: " << max_value << endl;
	#endif
}

void SphereFunction::clean_init( int _D, double _LB, double _UB )
{
	D  = _D;
	LB = _LB;
	UB = _UB;
	fitness_value = 0.0f;

	cudaMallocManaged(&x, D * sizeof(double));
	std::fill_n(x, D, static_cast<double>(0.0f));

	max_value = static_cast<double>( UB * UB * D );
	
	#if SPHERE_PRINT == 1
		std::cout << "SphereFunction defined with " << D << " dimensions" << std::endl;
		std::cout << "Lower-Bound: " << LB << std::endl;
		std::cout << "Upper-Bound: " << UB << std::endl;
		std::cout << "Max value reachable: " << max_value << std::endl;
	#endif
}

__device__ __host__ SphereFunction::~SphereFunction(void)
{
	/*if(x != NULL)
		delete [] x;*/
}

void SphereFunction::print()
{
	printf("Valor de Fitness: %.5lf\n", fitness_value);

	for( int i = 0; i < D; i++ ){
		std::cout << std::setprecision(3) << std::fixed << x[i] << " ";
	}
	std::cout << std::endl;
}

double SphereFunction::evaluate()
{
	double sum = 0.0f;
	for(int i = 0; i < D; i++)
	{
		sum += (x[i] * x[i]);
	}
	return sum;
}

double SphereFunction::fitness( double evaluated_value )
{
	double value = 1.0f - static_cast<double>( evaluated_value / max_value );
	setFitnessValue(value);
	return value;
}

__host__ __device__ void SphereFunction::verify_bounds()
{
	for( int i = 0; i < D; i++ )
	{
		if( x[i] > UB )
			x[i] = UB;
		
		if( x[i] < LB )
			x[i] = LB;
	}
}

__device__ __host__ void SphereFunction::setFitnessValue( const double& value )
{
	fitness_value = value;
}

__device__ __host__ double SphereFunction::getFitnessValue()
{
	return fitness_value;
}

__device__ __host__ double SphereFunction::getDimensionValue( const int& i )
{
	assert(i <= D);
	return x[i];
}

__device__ __host__ void SphereFunction::do_operations( const SphereFunction& a, const SphereFunction& b, const SphereFunction& c )
{
	double sum = 0.0f;

	for(int i = 0; i < D; i++) {
		x[i] = c.x[i] + 0.5 * (a.x[i] - b.x[i]);

		if( x[i] > UB )
			x[i] = UB;

		if( x[i] < LB )
			x[i] = LB;

		sum += (x[i] * x[i]);
	}

	setFitnessValue(static_cast<double>(1.0f - static_cast<double>( sum / max_value )));
}

