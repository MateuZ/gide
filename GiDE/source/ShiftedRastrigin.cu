#include "ShiftedRastrigin.cuh"

__host__ __device__  ShiftedRastrigin::ShiftedRastrigin()
{
  m_lowerBound = -5.12;
  m_upperBound =  5.12;
  m_fBias = -330.0;
  PI = acos(-1.0);
}

__host__ __device__ ShiftedRastrigin::~ShiftedRastrigin(){
  /* empty */
}

__host__ __device__  double ShiftedRastrigin::getOptimalPoint(){
  return -330.0;
}

double ShiftedRastrigin::evaluate(double individual[], int size)
{
	double z = 0;
	double Fx = 0;
	unsigned short int i;

	for(i = 0; i < size; i++)
	{
      z = individual[i] - rastrigin[i];
      Fx = Fx + ( pow(z,2) - 10*cos(2*PI*z) + 10);
	}
	Fx =  Fx + m_fBias;

	if(Fx - getOptimalPoint() <= 10e-20)
	{
		Fx = getOptimalPoint();
	}
	return Fx;
}

std::string ShiftedRastrigin::getName()
{
	return "ShiftedRastrigin";
}

__host__ __device__ double ShiftedRastrigin::verifyBounds(double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

__host__ __device__ double ShiftedRastrigin::getLowerBound()
{
	return m_lowerBound;
}

__host__ __device__ double ShiftedRastrigin::getUpperBound()
{
	return m_upperBound;
}
