#include "DE.cuh"

double stime(){
    struct timeval tv;
    struct timezone tz;
    gettimeofday(&tv, &tz);
    double mlsec = 1000.0 * ((double)tv.tv_sec + (double)tv.tv_usec/1000000.0);
    return mlsec/1000.0;
}

int rand_btw(int a, int b){
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> dist(a, b);
	return dist(gen);
}

__global__ void index_generator(int *v, int *vf, uint pop_size){
  unsigned short int ind = threadIdx.x;
  unsigned short int isl = blockIdx.x;
  unsigned short int stride = isl * pop_size * 3;

  curandState s;
  int n1, n2, n3;
  curand_init((ind + isl + 1) * clock(), 0, 0, &s);

  n1 = curand(&s) % pop_size;

  vf[ stride + ind ] = v[n1];
  if( v[n1] == ind ){
    n1 = (n1 + 1) % pop_size;
    vf[ stride + ind ] = v[n1];
  }
  n2 = (curand(&s) % (int)pop_size/3) + 1;

  vf[ stride + ind + pop_size] = v[(n1 + n2) % pop_size];
  if( v[ (n1 + n2) % pop_size ] == ind ){
    n2 = (n2 + 1) % pop_size;
    vf[stride + ind + pop_size] = v[(n1 + n2) % pop_size];
  }
  n3 = (curand(&s) % (int)pop_size/3) + 1;

  vf[stride + ind + 2*pop_size] = v[(n1 + n2 + n3) % pop_size];
  if( v[(n1 + n2 + n3) % pop_size] == ind ){
    n3 = (n3 + 1) % pop_size;
    vf[stride + ind + 2 * pop_size] = v[(n1 + n2 + n3) % pop_size];
  }
  //printf("[%d][%d] %d ~ %d ~  %d\n", isl, ind, v[n1] ,v[ (n1 + n2) % pop_size], v[ (n1 + n2 + n3) % pop_size]);
}

__global__ void subs_better_offspring(
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
  uint pop_size, uint n_dim
){
  int index = threadIdx.x;
  int isl   = blockIdx.x;
  int stride0 = isl * pop_size;
  int stride1 = isl * pop_size * n_dim;

  if(aux_evals[stride0 + index] <= pop_evals[stride0 + index]){
    for( int i = 0; i < n_dim; i++ ){
      pop_genes[ stride1 + (index * n_dim) + i ] = aux_genes[ stride1 + (index * n_dim) + i ];
    }
    pop_evals[stride0 + index] = aux_evals[stride0 + index];
  }
}

__global__ void DE_ShiftedGriewank(
	int *vf,
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
	double F, double CR, uint pop_size, uint n_dim//,
){
	int index = threadIdx.x;
  int isl   = blockIdx.x;
  int stride0 = isl * pop_size;
  int stride1 = isl * pop_size * 3;
  int stride2 = isl * pop_size * n_dim;

	int n1, n2, n3, rnbr;
  ShiftedGriewank sg;
  double z = 0;
  double top1 = 0;
  double top2 = 0;

  top1 = 0.0;
  top2 = 1.0;

  n1 = vf[stride1 + index];
  n2 = vf[stride1 + index + pop_size];
  n3 = vf[stride1 + index + (pop_size * 2)];
  //printf("(n1) %d (n2) %d (n3) %d\n", n1, n2, n3);

	curandState s;
	curand_init((index + isl + 1) * clock(), 0, 0, &s);

	rnbr = curand(&s)%n_dim;
	for( int k = 0; k < n_dim; k++ ){
		if( (curand_uniform(&s) <= CR) or (k == n_dim-1) ){

      aux_genes[stride2 + (index * n_dim) + rnbr] = pop_genes[stride2 + (n3 * n_dim) + rnbr] + F * (pop_genes[stride2 + (n1 * n_dim) + rnbr] - pop_genes[stride2 + (n2 * n_dim) + rnbr]);

			if( aux_genes[stride2 + (index * n_dim) + rnbr] > sg.getUpperBound() )
				aux_genes[stride2 + (index * n_dim) + rnbr] = sg.getUpperBound();

      if( aux_genes[stride2 + (index * n_dim) + rnbr] < sg.getLowerBound())
  			aux_genes[stride2 + (index * n_dim) + rnbr] = sg.getLowerBound();

		}else{
			//receive an equal dimension;
			aux_genes[stride2 + (index * n_dim) + rnbr] = pop_genes[stride2 + (index * n_dim) + rnbr];
		}
    z = aux_genes[stride2 + (index * n_dim) + rnbr] - sg.griewank[rnbr];
    top1 = top1 + static_cast<double>(( pow(z,2) / 4000.0 ));
    top2 = static_cast<double>(top2 * ( cos(z/sqrt(static_cast<double>(rnbr+1)))));

    rnbr = (rnbr+1)%n_dim;
	}

	top1 =  (top1 - top2 + 1.0 + (-180.0));
	if(top1 - sg.getOptimalPoint() <= 10e-8)	{
		top1 = sg.getOptimalPoint();
	}
	aux_evals[ stride0 + index] = top1;
}

__global__ void DE_ShiftedRosenbrock(
	int *vf,
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
	double F, double CR, uint pop_size, uint n_dim
){
	int index = threadIdx.x + blockDim.x * blockIdx.x;

	int n1, n2, n3, rnbr;

  ShiftedRosenbrock sr;
  double *zx = new double[n_dim];
  double Fx = 0.0;

  n1 = vf[index];
  n2 = vf[index + pop_size];
  n3 = vf[index + pop_size * 2];
  //printf("(n1) %d (n2) %d (n3) %d\n", n1, n2, n3);

	curandState s;
	curand_init((index + 1) * clock(), 0, 0, &s);

	rnbr = curand(&s)%n_dim;
	for( int k = 0; k < n_dim; k++ ){
		if( (curand_uniform(&s) <= CR) or (k == n_dim-1) ){

      aux_genes[(index * n_dim) + rnbr] = pop_genes[(n3 * n_dim) + rnbr] + F * (pop_genes[(n1 * n_dim) + rnbr] - pop_genes[(n2 * n_dim) + rnbr]);

			if( aux_genes[(index * n_dim) + rnbr] > +600.0f )
				aux_genes[(index * n_dim) + rnbr] = +600.0f;

      if( aux_genes[(index * n_dim) + rnbr] < -600.0f)
  			aux_genes[(index * n_dim) + rnbr] = -600.0f;

		}else{
			//receive an equal dimension;
			aux_genes[(index * n_dim) + rnbr] = pop_genes[(index * n_dim) + rnbr];
		}
    zx[rnbr] = static_cast<double>(aux_genes[(index * n_dim) + rnbr] - sr.rosenbrock[rnbr] + 1.0);
    rnbr = (rnbr+1)%n_dim;
	}

  for( int i = 0; i < n_dim - 1; i++ ){
    Fx += static_cast<double>(100.0*( pow((pow(zx[i],2)-zx[i+1]) , 2) ) + pow((zx[i]-1) , 2));
  }
	Fx += sr.m_fBias;

	if(Fx - sr.getOptimalPoint() <= 10e-8)	{
		Fx = sr.getOptimalPoint();
	}
	aux_evals[index] = Fx;
  free(zx);
}

__global__ void DE_ShiftedRastrigin(
	int *vf,
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
	double F, double CR, uint pop_size, uint n_dim//,
){
	int index = threadIdx.x + blockDim.x * blockIdx.x;

	int n1, n2, n3, rnbr;
  ShiftedRastrigin sr;
  double z = 0;
	double Fx = 0;

  n1 = vf[index];
  n2 = vf[index + pop_size];
  n3 = vf[index + pop_size * 2];

	curandState s;
	curand_init((index + 1) * clock(), 0, 0, &s);

	rnbr = curand(&s)%n_dim;
	for( int k = 0; k < n_dim; k++ ){
		if( (curand_uniform(&s) <= CR) or (k == n_dim-1) ){

      aux_genes[(index * n_dim) + rnbr] = pop_genes[(n3 * n_dim) + rnbr] + F * (pop_genes[(n1 * n_dim) + rnbr] - pop_genes[(n2 * n_dim) + rnbr]);

			if( aux_genes[(index * n_dim) + rnbr] > sr.getUpperBound() )
				aux_genes[(index * n_dim) + rnbr] = sr.getUpperBound();

      if( aux_genes[(index * n_dim) + rnbr] < sr.getLowerBound() )
  			aux_genes[(index * n_dim) + rnbr] = sr.getLowerBound();

		}else{
			//receive an equal dimension;
			aux_genes[(index * n_dim) + rnbr] = pop_genes[(index * n_dim) + rnbr];
		}
    z = aux_genes[(index * n_dim) + rnbr] - sr.rastrigin[rnbr];
    Fx = Fx + static_cast<double>(( pow(z, 2) - 10.0*cos(2.0*sr.PI*z) + 10.0 ));

    rnbr = (rnbr+1)%n_dim;
	}
  Fx =  Fx + sr.m_fBias;
  if(Fx - sr.getOptimalPoint() <= 10e-8)
		Fx = sr.getOptimalPoint();

	aux_evals[index] = Fx;
}

__global__ void DE_ShiftedSphere(
	int *vf,
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
	double F, double CR, uint pop_size, uint n_dim//,
){
  int index = threadIdx.x + blockDim.x * blockIdx.x;

	int n1, n2, n3, rnbr;
  ShiftedSphere ss;
  double z = 0;
	double Fx = 0;

  n1 = vf[index];
  n2 = vf[index + pop_size];
  n3 = vf[index + pop_size * 2];

	curandState s;
	curand_init((index + 1) * clock(), 0, 0, &s);

	rnbr = curand(&s)%n_dim;
	for( int k = 0; k < n_dim; k++ ){
		if( (curand_uniform(&s) <= CR) or (k == n_dim-1) ){

      aux_genes[(index * n_dim) + rnbr] = pop_genes[(n3 * n_dim) + rnbr] + F * (pop_genes[(n1 * n_dim) + rnbr] - pop_genes[(n2 * n_dim) + rnbr]);

			if( aux_genes[(index * n_dim) + rnbr] > ss.getUpperBound() )
				aux_genes[(index * n_dim) + rnbr] = ss.getUpperBound();

      if( aux_genes[(index * n_dim) + rnbr] < ss.getLowerBound())
  			aux_genes[(index * n_dim) + rnbr] = ss.getLowerBound();

		}else{
			//receive an equal dimension;
			aux_genes[(index * n_dim) + rnbr] = pop_genes[(index * n_dim) + rnbr];
		}
    z = aux_genes[(index * n_dim) + rnbr] - ss.sphere[rnbr];
    Fx += z*z;

    rnbr = (rnbr+1)%n_dim;
	}
  Fx =  Fx + ss.m_fBias;
  if(Fx - ss.getOptimalPoint() <= 10e-8)
		Fx = ss.getOptimalPoint();

	aux_evals[index] = Fx;
}

__global__ void griewank_eval(
  double *aux_genes, double *aux_evals,
  uint pop_size, uint n_dim)
{

  int index = threadIdx.x;
  int isl   = blockIdx.x;

  int stride = isl * pop_size;

  ShiftedGriewank sg;
  double z = 0;
	double top1 = 0;
	double top2 = 0;
	unsigned short int i;

	top1 = 0.0;
	top2 = 1.0;
	for(i = 0; i < n_dim; i++){
		z = aux_genes[stride + (index * n_dim) + i] - sg.griewank[i];
		top1 = top1 + static_cast<double>(( pow(z,2) / 4000 ));
		top2 = static_cast<double>(top2 * ( cos(z/sqrt(static_cast<double>(i+1)))));
	}
	top1 =  (top1 - top2 + 1.0 + (-180.0));
	if(top1 - sg.getOptimalPoint() <= 10e-8)	{
		top1 = sg.getOptimalPoint();
	}
	aux_evals[stride + index] = top1;
}

__global__ void rastrigin_eval(
  double *aux_genes, double *aux_evals,
  uint pop_size, uint n_dim)
{

  int index = threadIdx.x + blockDim.x * blockIdx.x;

  ShiftedRastrigin sg;
  double z = 0;
	double Fx = 0;

	for(int i = 0; i < n_dim; i++){
		z = aux_genes[(index * n_dim) + i] - sg.rastrigin[i];
    Fx = Fx + ( pow(z,2) - 10*cos(2*sg.PI*z) + 10);
	}
	Fx =  Fx + sg.m_fBias;
	if( Fx - sg.getOptimalPoint() <= 10e-8)	{
		Fx = sg.getOptimalPoint();
	}
	aux_evals[index] = Fx;
}

__global__ void rosenbrock_eval(
  double *aux_genes, double *aux_evals,
  uint pop_size, uint n_dim)
{
  int index = threadIdx.x + blockDim.x * blockIdx.x;

  double *zx = new double[n_dim];
  double Fx = 0.0f;

  ShiftedRosenbrock sg;

  for(int i = 0; i < n_dim; i++)
    zx[i] = aux_genes[((index * n_dim) + i)] - sg.rosenbrock[i] + 1;

	for(int i = 0; i < n_dim-1; i++)
		Fx += 100*( pow((pow(zx[i],2)-zx[i+1]) , 2) ) + pow((zx[i]-1) , 2);

	Fx += sg.m_fBias;
	if(Fx - sg.getOptimalPoint() <= 10e-8)	{
		Fx = sg.getOptimalPoint();
	}
	aux_evals[index] = Fx;

  free(zx);
}

__global__ void sphere_eval(
  double *aux_genes, double *aux_evals,
  uint pop_size, uint n_dim)
{

  int index = threadIdx.x + blockDim.x * blockIdx.x;

  ShiftedSphere sg;
  double z = 0;
	double Fx = 0.0f;

	for(int i = 0; i < n_dim; i++){
		z = aux_genes[(index * n_dim) + i] - sg.sphere[i];
		Fx += z*z;
	}
	Fx += sg.m_fBias;
	if(Fx - sg.getOptimalPoint() <= 10e-8)	{
		Fx = sg.getOptimalPoint();
	}
	aux_evals[index] = Fx;
}

__global__ void migration(double *pop_genes, double *pop_evals, uint n_islands, uint pop_size, uint n_dim)
{
  int isl = blockIdx.x;

  int stride = isl * pop_size;
  int stride2 = ((isl+1)%n_islands)*pop_size;

  double *a1 = thrust::max_element(thrust::device, pop_evals + stride, pop_evals + stride + pop_size );
  int pos_worst = a1 - (pop_evals + stride);

  double *a2 = thrust::min_element(thrust::device, pop_evals + stride2, pop_evals + stride2 + pop_size);
  int pos_best = a2 - (pop_evals + stride2);

  double *dest   = pop_genes + (pop_size * n_dim * isl) + (pos_worst * n_dim);
  double *source = pop_genes + (pop_size * n_dim * ( (isl+1)%n_islands ) ) + (pos_best * n_dim);

  for( int o = 0; o < n_dim; o++, dest++, source++ )
    *dest = *source;

  *a1 = *a2;
}
std::tuple<double, double, double> DE_execute(
  uint pop_size, uint n_ger, uint n_dim,
  double mf, double cr,
  std::string FuncObj, uint n_islands
){

  /* GPU */
  int_dV v_gpu(pop_size);
  int_dV vf_gpu(n_islands * pop_size * 3);
  /*
   * Similar to C++ STL function std::iota
   * Reference: https://goo.gl/mcpZfs
  */
  thrust::sequence(thrust::device, v_gpu.begin(), v_gpu.end());

  /*
  thrust::host_vector<int> h(n_islands * pop_size * 3);
  h = vf_gpu;
  for(int i = 0; i < n_islands; i++ ){
    int stride = i * pop_size * 3;
    printf("Ilha #%d\n", i );
    for( int j = 0; j < pop_size; j++ ){
      printf("Ind @%d %d %d %d\n", j, h[stride + j], h[stride + j + pop_size], h[stride + j + 2*pop_size]);
    }
  }*/

  double_dV pop_genes(n_islands * pop_size * n_dim);
  double_dV pop_evals(n_islands * pop_size);
  double_dV aux_genes(n_islands * pop_size * n_dim);
  double_dV aux_evals(n_islands * pop_size);
  thrust::fill(thrust::device, pop_evals.begin(), pop_evals.end(), 0.0f);
  thrust::fill(thrust::device, aux_evals.begin(), aux_evals.end(), 0.0f);
  thrust::fill(thrust::device, aux_genes.begin(), aux_genes.end(), 0.0f);

  double best = 0.00f, EFV = 0.00f;
  float time = 0.00f;

  if( FuncObj == "shifted_griewank"){

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    thrust::host_vector<double> hv(n_islands * pop_size * n_dim);

    std::mt19937 rng;
    rng.seed(std::random_device{}());
    std::uniform_real_distribution<double> d(-600.00, +600.00);

    for( int i = 0; i < (n_islands * pop_size * n_dim); i++ )
      hv[i] = d(rng);

    pop_genes = hv;

    ShiftedGriewank sg;

    cudaEventRecord(start);

    griewank_eval<<<n_islands, pop_size>>>(
      thrust::raw_pointer_cast(&pop_genes[0]),
      thrust::raw_pointer_cast(&pop_evals[0]),
      pop_size, n_dim
    );
    for(int i = 0; i < n_ger; i++){
      index_generator<<<n_islands, pop_size>>>(thrust::raw_pointer_cast(&v_gpu[0]), thrust::raw_pointer_cast(&vf_gpu[0]), pop_size);
      DE_ShiftedGriewank<<<n_islands, pop_size>>>(
        thrust::raw_pointer_cast(&vf_gpu[0]),
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        mf, cr, pop_size, n_dim
      );
      subs_better_offspring<<<n_islands, pop_size>>>(
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        pop_size, n_dim);

      if( (i % 10 == 0) && (n_islands > 1) ){
        migration<<<n_islands, 1>>>(
          thrust::raw_pointer_cast(&pop_genes[0]),
          thrust::raw_pointer_cast(&pop_evals[0]),
          n_islands, pop_size, n_dim
        );
        //cudaDeviceSynchronize();
        /*  double_dV_it it_A = pop_evals.begin();
          //for( int j = 0; j < n_islands; j++ ){
          //  printf("Ilha %d:\n", j);
          //  for( int k = 0; k < pop_size; k++, it_A++){
          //    printf(" %.2lf ", static_cast<double>(*it_A));
          //  }
          //  printf("\n");
          //}

        for( int j = 0; j < n_islands; j++ ){
          int stride = j * pop_size;
          int stride2 = ((j+1)%n_islands)*pop_size;
          double_dV_it a1 = thrust::max_element(thrust::device, pop_evals.begin() + stride, pop_evals.begin() + stride + pop_size );
          int pos_worst = a1 - (pop_evals.begin() + stride);
          double val_worst = *a1;
          //printf("[%d] %.2lf\n", pos_worst, val_worst);

          double_dV_it a2 = thrust::min_element(thrust::device, pop_evals.begin() + stride2, pop_evals.begin() + stride2 + pop_size);
          int pos_best = a2 - (pop_evals.begin() + stride2);
          double val_best = *a2;
          //printf("Will receive this %.2lf from island %d\n", val_best, (j+1)%n_islands );


          double_dV_it dest = pop_genes.begin() + (pop_size * n_dim * j) + (pos_worst * n_dim);
          double_dV_it source = pop_genes.begin() + (pop_size * n_dim * ( (j+1)%n_islands ) ) + (pos_best * n_dim);

          for( int o = 0; o < n_dim; o++, dest++, source++ ){
            //printf("%.2lf = %.2lf\n", static_cast<double>(*dest), static_cast<double>(*source));
            *dest = *source;
          }
          dest = pop_genes.begin() + (pop_size * n_dim * j) + (pos_worst * n_dim);
          //for( int o = 0; o < n_dim; o++, dest++ ){
          //  printf("%.2lf ", static_cast<double>(*dest));
          //}
          printf("\n");
          *a1 = *a2;
        }*/
      }
    }
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    cudaEventElapsedTime(&time, start, stop);
    time /= 1000.0f;
    double_dV_it it = pop_evals.begin();
    it = thrust::min_element(thrust::device, pop_evals.begin(), pop_evals.end());
    best = static_cast<double>(*it);
    EFV = fabs(best - (-180.00f));

  }/*else if( FuncObj == "shifted_sphere" ){
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    thrust::host_vector<double> hv(pop_size * n_dim);

    std::mt19937 rng;
    rng.seed(std::random_device{}());
    std::uniform_real_distribution<double> d(-5.00, +5.00);

    for( int i = 0; i < (pop_size * n_dim); i++ )
      hv[i] = d(rng);

    pop_genes = hv;

    cudaEventRecord(start);

    sphere_eval<<<32, 128>>>(
      thrust::raw_pointer_cast(&pop_genes[0]),
      thrust::raw_pointer_cast(&pop_evals[0]),
      pop_size, n_dim
    );
    for(int i = 0; i < n_ger; i++){
      index_generator<<<32, 128>>>(thrust::raw_pointer_cast(&v_gpu[0]), thrust::raw_pointer_cast(&vf_gpu[0]), pop_size);
      DE_ShiftedSphere<<<32, 128>>>(
        thrust::raw_pointer_cast(&vf_gpu[0]),
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        mf, cr, pop_size, n_dim
      );
      subs_better_offspring<<<32, 128>>>(
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        pop_size, n_dim);
    }
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    cudaEventElapsedTime(&time, start, stop);
    time /= 1000.0f;
    double_dV_it it = pop_evals.begin();
    best = static_cast<double>(*it);
    for(; it != pop_evals.end(); it++){
      if( static_cast<double>(*it) < best ){
        best = static_cast<double>(*it);
      }
    }
    EFV = fabs(fabs(best) - 450.00f);
  }
  else if( FuncObj == "shifted_rastrigin" )
  {
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    thrust::host_vector<double> hv(pop_size * n_dim);

    std::mt19937 rng;
    rng.seed(std::random_device{}());
    std::uniform_real_distribution<double> d(-5.00, +5.00);

    for( int i = 0; i < (pop_size * n_dim); i++ )
      hv[i] = d(rng);

    pop_genes = hv;

    cudaEventRecord(start);

    rastrigin_eval<<<32, 128>>>(
      thrust::raw_pointer_cast(&pop_genes[0]),
      thrust::raw_pointer_cast(&pop_evals[0]),
      pop_size, n_dim
    );
    for(int i = 0; i < n_ger; i++){
      index_generator<<<32, 128>>>(thrust::raw_pointer_cast(&v_gpu[0]), thrust::raw_pointer_cast(&vf_gpu[0]), pop_size);
      DE_ShiftedRastrigin<<<32, 128>>>(
        thrust::raw_pointer_cast(&vf_gpu[0]),
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        mf, cr, pop_size, n_dim
      );
      subs_better_offspring<<<32, 128>>>(
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        pop_size, n_dim);
    }
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    cudaEventElapsedTime(&time, start, stop);
    time /= 1000.0f;
    double_dV_it it = pop_evals.begin();
    best = static_cast<double>(*it);
    for(; it != pop_evals.end(); it++){
      if( static_cast<double>(*it) < best ){
        best = static_cast<double>(*it);
      }
    }
    EFV = fabs(fabs(best) - 330.00f);
  }
  else if( FuncObj == "shifted_rosenbrock"){
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    thrust::host_vector<double> hv(pop_size * n_dim);

    std::mt19937 rng;
    rng.seed(std::random_device{}());
    std::uniform_real_distribution<double> d(-5.00, +5.00);

    for( int i = 0; i < (pop_size * n_dim); i++ )
      hv[i] = d(rng);

    pop_genes = hv;

    cudaEventRecord(start);

    rosenbrock_eval<<<32, 128>>>(
      thrust::raw_pointer_cast(&pop_genes[0]),
      thrust::raw_pointer_cast(&pop_evals[0]),
      pop_size, n_dim
    );
    for(int i = 0; i < n_ger; i++){
      index_generator<<<32, 128>>>(thrust::raw_pointer_cast(&v_gpu[0]), thrust::raw_pointer_cast(&vf_gpu[0]), pop_size);
      DE_ShiftedRosenbrock<<<32, 128>>>(
        thrust::raw_pointer_cast(&vf_gpu[0]),
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        mf, cr, pop_size, n_dim
      );
      subs_better_offspring<<<32, 128>>>(
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        pop_size, n_dim);
    }
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    cudaEventElapsedTime(&time, start, stop);
    time /= 1000.0f;
    double_dV_it it = pop_evals.begin();
    best = static_cast<double>(*it);
    for(; it != pop_evals.end(); it++){
      if( static_cast<double>(*it) < best ){
        best = static_cast<double>(*it);
      }
    }
    EFV = fabs(fabs(best) - 390.00f);
  }*/
  /*CODE END*/
  return std::make_tuple(best, time, EFV);
}
