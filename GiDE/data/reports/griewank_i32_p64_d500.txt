+==============================================================+
|                          CONFIGURAÇÃO                        |

+==============================================================+
| Número de execuções:      1
| Tamanho da População:     64
| Número de Gerações:       20000
| Número de Dimensões:      500
| Fator de Mutação (F):     0.50
| Taxa de Crossover:        0.90
| Função Objetivo:          shifted_griewank
| Número de Ilhas:          32
+==============================================================+
| Execução: 1  Melhor Global: -179.989(0.011) Tempo de Execução (s): 108.276
+==============================================================+
|                   RESULTADOS DOS EXPERIMENTOS                |
+==============================================================+
| Função Objetivo:
|       Média:                 -179.989
|       Devio-Padrão:          +0.000
| Erro Função Objetivo (EFV)
|       Média:                 +0.011
|       Desvio-Padrão:         +0.000
| Tempo de Execução:
|       Média:                 +108.276
|       Devio-Padrão:          +0.000
+==============================================================+
