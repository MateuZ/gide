mateus@Bagual:~/Documentos/CN-PF/bb-git/GiDE$ ./GiDE-app -r 10 -p 32 -i 1 -g 2000 -d 500 -o shifted_griewank
 +==============================================================+
 |                          CONFIGURAÇÃO                        |
 +==============================================================+
 | Número de execuções:      10
 | Tamanho da População:     32
 | Número de Gerações:       2000
 | Número de Dimensões:      500
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.90
 | Função Objetivo:          shifted_griewank
 | Número de Ilhas:          1
 +==============================================================+
 | Execução: 1  Melhor Global: +4108.438(4288.438) Tempo de Execução (s): 8.849
 | Execução: 2  Melhor Global: +3885.068(4065.068) Tempo de Execução (s): 8.797
 | Execução: 3  Melhor Global: +3503.830(3683.830) Tempo de Execução (s): 8.767
 | Execução: 4  Melhor Global: +3608.838(3788.838) Tempo de Execução (s): 8.769
 | Execução: 5  Melhor Global: +3542.790(3722.790) Tempo de Execução (s): 8.767
 | Execução: 6  Melhor Global: +3685.214(3865.214) Tempo de Execução (s): 8.765
 | Execução: 7  Melhor Global: +4047.516(4227.516) Tempo de Execução (s): 8.768
 | Execução: 8  Melhor Global: +3437.436(3617.436) Tempo de Execução (s): 8.768
 | Execução: 9  Melhor Global: +3455.543(3635.543) Tempo de Execução (s): 8.771
 | Execução: 10 Melhor Global: +3363.885(3543.885) Tempo de Execução (s): 8.770
 +==============================================================+
 |                   RESULTADOS DOS EXPERIMENTOS                |
 +==============================================================+
 | Função Objetivo:
 | 	 Média:                 +3663.856
 | 	 Devio-Padrão:          +249.434
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +3843.856
 | 	 Desvio-Padrão:         +249.434
 | Tempo de Execução:
 | 	 Média:                 +8.779
 | 	 Devio-Padrão:          +0.025
 +==============================================================+
mateus@Bagual:~/Documentos/CN-PF/bb-git/GiDE$ ./GiDE-app -r 10 -p 32 -i 8 -g 2000 -d 500 -o shifted_griewank
 +==============================================================+
 |                          CONFIGURAÇÃO                        |
 +==============================================================+
 | Número de execuções:      10
 | Tamanho da População:     32
 | Número de Gerações:       2000
 | Número de Dimensões:      500
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.90
 | Função Objetivo:          shifted_griewank
 | Número de Ilhas:          8
 +==============================================================+
 | Execução: 1  Melhor Global: -157.650(22.350) Tempo de Execução (s): 9.592
 | Execução: 2  Melhor Global: -153.054(26.946) Tempo de Execução (s): 9.538
 | Execução: 3  Melhor Global: -156.013(23.987) Tempo de Execução (s): 9.545
 | Execução: 4  Melhor Global: -149.871(30.129) Tempo de Execução (s): 9.540
 | Execução: 5  Melhor Global: -143.815(36.185) Tempo de Execução (s): 9.546
 | Execução: 6  Melhor Global: -157.686(22.314) Tempo de Execução (s): 9.543
 | Execução: 7  Melhor Global: -157.729(22.271) Tempo de Execução (s): 9.546
 | Execução: 8  Melhor Global: -128.566(51.434) Tempo de Execução (s): 9.542
 | Execução: 9  Melhor Global: -155.600(24.400) Tempo de Execução (s): 9.552
 | Execução: 10 Melhor Global: -151.191(28.809) Tempo de Execução (s): 9.546
 +==============================================================+
 |                   RESULTADOS DOS EXPERIMENTOS                |
 +==============================================================+
 | Função Objetivo:
 | 	 Média:                 -151.118
 | 	 Devio-Padrão:          +8.607
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +28.882
 | 	 Desvio-Padrão:         +8.607
 | Tempo de Execução:
 | 	 Média:                 +9.549
 | 	 Devio-Padrão:          +0.015
 +==============================================================+
mateus@Bagual:~/Documentos/CN-PF/bb-git/GiDE$ ./GiDE-app -r 10 -p 32 -i 16 -g 2000 -d 500 -o shifted_griewank
 +==============================================================+
 |                          CONFIGURAÇÃO                        |
 +==============================================================+
 | Número de execuções:      10
 | Tamanho da População:     32
 | Número de Gerações:       2000
 | Número de Dimensões:      500
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.90
 | Função Objetivo:          shifted_griewank
 | Número de Ilhas:          16
 +==============================================================+
 | Execução: 1  Melhor Global: -171.768(8.232) Tempo de Execução (s): 9.868
 | Execução: 2  Melhor Global: -171.247(8.753) Tempo de Execução (s): 9.840
 | Execução: 3  Melhor Global: -170.680(9.320) Tempo de Execução (s): 9.846
 | Execução: 4  Melhor Global: -171.575(8.425) Tempo de Execução (s): 9.842
 | Execução: 5  Melhor Global: -170.790(9.210) Tempo de Execução (s): 9.849
 | Execução: 6  Melhor Global: -171.813(8.187) Tempo de Execução (s): 9.848
 | Execução: 7  Melhor Global: -171.805(8.195) Tempo de Execução (s): 9.842
 | Execução: 8  Melhor Global: -171.669(8.331) Tempo de Execução (s): 9.849
 | Execução: 9  Melhor Global: -172.405(7.595) Tempo de Execução (s): 9.853
 | Execução: 10 Melhor Global: -171.405(8.595) Tempo de Execução (s): 9.847
 +==============================================================+
 |                   RESULTADOS DOS EXPERIMENTOS                |
 +==============================================================+
 | Função Objetivo:
 | 	 Média:                 -171.515
 | 	 Devio-Padrão:          +0.486
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +8.485
 | 	 Desvio-Padrão:         +0.486
 | Tempo de Execução:
 | 	 Média:                 +9.848
 | 	 Devio-Padrão:          +0.008
 +==============================================================+
mateus@Bagual:~/Documentos/CN-PF/bb-git/GiDE$ ./GiDE-app -r 10 -p 32 -i 32 -g 2000 -d 500 -o shifted_griewank
 +==============================================================+
 |                          CONFIGURAÇÃO                        |
 +==============================================================+
 | Número de execuções:      10
 | Tamanho da População:     32
 | Número de Gerações:       2000
 | Número de Dimensões:      500
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.90
 | Função Objetivo:          shifted_griewank
 | Número de Ilhas:          32
 +==============================================================+
 | Execução: 1  Melhor Global: -173.192(6.808) Tempo de Execução (s): 10.112
 | Execução: 2  Melhor Global: -173.204(6.796) Tempo de Execução (s): 10.060
 | Execução: 3  Melhor Global: -173.045(6.955) Tempo de Execução (s): 10.060
 | Execução: 4  Melhor Global: -173.006(6.994) Tempo de Execução (s): 10.062
 | Execução: 5  Melhor Global: -173.430(6.570) Tempo de Execução (s): 10.062
 | Execução: 6  Melhor Global: -172.826(7.174) Tempo de Execução (s): 10.062
 | Execução: 7  Melhor Global: -173.445(6.555) Tempo de Execução (s): 10.065
 | Execução: 8  Melhor Global: -173.215(6.785) Tempo de Execução (s): 10.064
 | Execução: 9  Melhor Global: -173.483(6.517) Tempo de Execução (s): 10.065
 | Execução: 10 Melhor Global: -172.986(7.014) Tempo de Execução (s): 10.066
 +==============================================================+
 |                   RESULTADOS DOS EXPERIMENTOS                |
 +==============================================================+
 | Função Objetivo:
 | 	 Média:                 -173.183
 | 	 Devio-Padrão:          +0.209
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +6.817
 | 	 Desvio-Padrão:         +0.209
 | Tempo de Execução:
 | 	 Média:                 +10.068
 | 	 Devio-Padrão:          +0.015
 +==============================================================+
mateus@Bagual:~/Documentos/CN-PF/bb-git/GiDE$ ./GiDE-app -r 10 -p 64 -i 1 -g 2000 -d 500 -o shifted_griewank
 +==============================================================+
 |                          CONFIGURAÇÃO                        |
 +==============================================================+
 | Número de execuções:      10
 | Tamanho da População:     64
 | Número de Gerações:       2000
 | Número de Dimensões:      500
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.90
 | Função Objetivo:          shifted_griewank
 | Número de Ilhas:          1
 +==============================================================+
 | Execução: 1  Melhor Global: +949.985(1129.985) Tempo de Execução (s): 9.096
 | Execução: 2  Melhor Global: +852.757(1032.757) Tempo de Execução (s): 8.850
 | Execução: 3  Melhor Global: +855.905(1035.905) Tempo de Execução (s): 9.040
 | Execução: 4  Melhor Global: +1041.048(1221.048) Tempo de Execução (s): 8.854
 | Execução: 5  Melhor Global: +764.989(944.989) Tempo de Execução (s): 9.038
 | Execução: 6  Melhor Global: +1161.618(1341.618) Tempo de Execução (s): 8.850
 | Execução: 7  Melhor Global: +823.830(1003.830) Tempo de Execução (s): 9.039
 | Execução: 8  Melhor Global: +1023.583(1203.583) Tempo de Execução (s): 8.850
 | Execução: 9  Melhor Global: +1071.563(1251.563) Tempo de Execução (s): 9.040
 | Execução: 10 Melhor Global: +1006.686(1186.686) Tempo de Execução (s): 8.854
 +==============================================================+
 |                   RESULTADOS DOS EXPERIMENTOS                |
 +==============================================================+
 | Função Objetivo:
 | 	 Média:                 +955.196
 | 	 Devio-Padrão:          +120.277
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +1135.196
 | 	 Desvio-Padrão:         +120.277
 | Tempo de Execução:
 | 	 Média:                 +8.951
 | 	 Devio-Padrão:          +0.101
 +==============================================================+
mateus@Bagual:~/Documentos/CN-PF/bb-git/GiDE$ ./GiDE-app -r 10 -p 64 -i 8 -g 2000 -d 500 -o shifted_griewank
 +==============================================================+
 |                          CONFIGURAÇÃO                        |
 +==============================================================+
 | Número de execuções:      10
 | Tamanho da População:     64
 | Número de Gerações:       2000
 | Número de Dimensões:      500
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.90
 | Função Objetivo:          shifted_griewank
 | Número de Ilhas:          8
 +==============================================================+
 | Execução: 1  Melhor Global: -169.891(10.109) Tempo de Execução (s): 9.908
 | Execução: 2  Melhor Global: -171.551(8.449) Tempo de Execução (s): 9.861
 | Execução: 3  Melhor Global: -170.735(9.265) Tempo de Execução (s): 9.871
 | Execução: 4  Melhor Global: -171.955(8.045) Tempo de Execução (s): 9.866
 | Execução: 5  Melhor Global: -172.526(7.474) Tempo de Execução (s): 9.871
 | Execução: 6  Melhor Global: -171.975(8.025) Tempo de Execução (s): 9.867
 | Execução: 7  Melhor Global: -173.011(6.989) Tempo de Execução (s): 9.876
 | Execução: 8  Melhor Global: -171.416(8.584) Tempo de Execução (s): 9.872
 | Execução: 9  Melhor Global: -172.076(7.924) Tempo de Execução (s): 9.872
 | Execução: 10 Melhor Global: -172.079(7.921) Tempo de Execução (s): 9.870
 +==============================================================+
 |                   RESULTADOS DOS EXPERIMENTOS                |
 +==============================================================+
 | Função Objetivo:
 | 	 Média:                 -171.721
 | 	 Devio-Padrão:          +0.844
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +8.279
 | 	 Desvio-Padrão:         +0.844
 | Tempo de Execução:
 | 	 Média:                 +9.873
 | 	 Devio-Padrão:          +0.012
 +==============================================================+
mateus@Bagual:~/Documentos/CN-PF/bb-git/GiDE$ ./GiDE-app -r 10 -p 64 -i 16 -g 2000 -d 500 -o shifted_griewank
 +==============================================================+
 |                          CONFIGURAÇÃO                        |
 +==============================================================+
 | Número de execuções:      10
 | Tamanho da População:     64
 | Número de Gerações:       2000
 | Número de Dimensões:      500
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.90
 | Função Objetivo:          shifted_griewank
 | Número de Ilhas:          16
 +==============================================================+
 | Execução: 1  Melhor Global: -175.475(4.525) Tempo de Execução (s): 10.077
 | Execução: 2  Melhor Global: -175.678(4.322) Tempo de Execução (s): 10.053
 | Execução: 3  Melhor Global: -175.590(4.410) Tempo de Execução (s): 10.061
 | Execução: 4  Melhor Global: -176.152(3.848) Tempo de Execução (s): 10.054
 | Execução: 5  Melhor Global: -175.632(4.368) Tempo de Execução (s): 10.062
 | Execução: 6  Melhor Global: -176.327(3.673) Tempo de Execução (s): 10.056
 | Execução: 7  Melhor Global: -175.699(4.301) Tempo de Execução (s): 10.064
 | Execução: 8  Melhor Global: -175.653(4.347) Tempo de Execução (s): 10.059
 | Execução: 9  Melhor Global: -175.475(4.525) Tempo de Execução (s): 10.064
 | Execução: 10 Melhor Global: -175.608(4.392) Tempo de Execução (s): 10.059
 +==============================================================+
 |                   RESULTADOS DOS EXPERIMENTOS                |
 +==============================================================+
 | Função Objetivo:
 | 	 Média:                 -175.729
 | 	 Devio-Padrão:          +0.268
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +4.271
 | 	 Desvio-Padrão:         +0.268
 | Tempo de Execução:
 | 	 Média:                 +10.061
 | 	 Devio-Padrão:          +0.006
 +==============================================================+
mateus@Bagual:~/Documentos/CN-PF/bb-git/GiDE$ ./GiDE-app -r 10 -p 64 -i 32 -g 2000 -d 500 -o shifted_griewank
  +==============================================================+
  |                          CONFIGURAÇÃO                        |
  +==============================================================+
  | Número de execuções:      10
  | Tamanho da População:     64
  | Número de Gerações:       2000
  | Número de Dimensões:      500
  | Fator de Mutação (F):     0.50
  | Taxa de Crossover:        0.90
  | Função Objetivo:          shifted_griewank
  | Número de Ilhas:          32
  +==============================================================+
  | Execução: 1  Melhor Global: -175.839(4.161) Tempo de Execução (s): 11.598
  | Execução: 2  Melhor Global: -176.170(3.830) Tempo de Execução (s): 11.571
  | Execução: 3  Melhor Global: -176.169(3.831) Tempo de Execução (s): 11.576
  | Execução: 4  Melhor Global: -175.397(4.603) Tempo de Execução (s): 11.575
  | Execução: 5  Melhor Global: -175.909(4.091) Tempo de Execução (s): 11.581
  | Execução: 6  Melhor Global: -175.861(4.139) Tempo de Execução (s): 11.573
  | Execução: 7  Melhor Global: -176.122(3.878) Tempo de Execução (s): 11.583
  | Execução: 8  Melhor Global: -175.901(4.099) Tempo de Execução (s): 11.574
  | Execução: 9  Melhor Global: -176.152(3.848) Tempo de Execução (s): 11.585
  | Execução: 10 Melhor Global: -176.190(3.810) Tempo de Execução (s): 11.593
  +==============================================================+
  |                   RESULTADOS DOS EXPERIMENTOS                |
  +==============================================================+
  | Função Objetivo:
  | 	 Média:                 -175.971
  | 	 Devio-Padrão:          +0.234
  | Erro Função Objetivo (EFV)
  | 	 Média:                 +4.029
  | 	 Desvio-Padrão:         +0.234
  | Tempo de Execução:
  | 	 Média:                 +11.581
  | 	 Devio-Padrão:          +0.009
  +==============================================================+
