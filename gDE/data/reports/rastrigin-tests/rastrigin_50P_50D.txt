 +==============================================================+ 
 |                          CONFIGURAÇÃO                        |
 +==============================================================+ 
 | Número de execuções:      30
 | Tamanho da População:     50
 | Número de Gerações:       10000
 | Número de Dimensões:      50
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.30
 | Função Objetivo:          shifted_rastrigin
 +==============================================================+ 
 | Execução: 1  Melhor Global: -209.284(120.716) Tempo de Execução (s): 15.752
 | Execução: 2  Melhor Global: -211.256(118.744) Tempo de Execução (s): 15.644
 | Execução: 3  Melhor Global: -203.122(126.878) Tempo de Execução (s): 15.639
 | Execução: 4  Melhor Global: -214.791(115.209) Tempo de Execução (s): 15.639
 | Execução: 5  Melhor Global: -219.474(110.526) Tempo de Execução (s): 15.639
 | Execução: 6  Melhor Global: -209.611(120.389) Tempo de Execução (s): 15.642
 | Execução: 7  Melhor Global: -206.431(123.569) Tempo de Execução (s): 15.641
 | Execução: 8  Melhor Global: -208.583(121.417) Tempo de Execução (s): 15.643
 | Execução: 9  Melhor Global: -213.995(116.005) Tempo de Execução (s): 15.643
 | Execução: 10 Melhor Global: -207.527(122.473) Tempo de Execução (s): 15.647
 | Execução: 11 Melhor Global: -204.147(125.853) Tempo de Execução (s): 15.648
 | Execução: 12 Melhor Global: -221.956(108.044) Tempo de Execução (s): 15.647
 | Execução: 13 Melhor Global: -215.531(114.469) Tempo de Execução (s): 15.646
 | Execução: 14 Melhor Global: -224.986(105.014) Tempo de Execução (s): 15.646
 | Execução: 15 Melhor Global: -206.477(123.523) Tempo de Execução (s): 15.648
 | Execução: 16 Melhor Global: -206.719(123.281) Tempo de Execução (s): 15.649
 | Execução: 17 Melhor Global: -215.371(114.629) Tempo de Execução (s): 15.647
 | Execução: 18 Melhor Global: -213.349(116.651) Tempo de Execução (s): 15.649
 | Execução: 19 Melhor Global: -204.539(125.461) Tempo de Execução (s): 15.651
 | Execução: 20 Melhor Global: -208.445(121.555) Tempo de Execução (s): 15.647
 | Execução: 21 Melhor Global: -216.749(113.251) Tempo de Execução (s): 15.647
 | Execução: 22 Melhor Global: -202.978(127.022) Tempo de Execução (s): 15.647
 | Execução: 23 Melhor Global: -209.868(120.132) Tempo de Execução (s): 15.648
 | Execução: 24 Melhor Global: -200.837(129.163) Tempo de Execução (s): 15.648
 | Execução: 25 Melhor Global: -192.770(137.230) Tempo de Execução (s): 15.649
 | Execução: 26 Melhor Global: -218.491(111.509) Tempo de Execução (s): 15.650
 | Execução: 27 Melhor Global: -206.230(123.770) Tempo de Execução (s): 15.652
 | Execução: 28 Melhor Global: -199.218(130.782) Tempo de Execução (s): 15.651
 | Execução: 29 Melhor Global: -224.030(105.970) Tempo de Execução (s): 15.654
 | Execução: 30 Melhor Global: -203.685(126.315) Tempo de Execução (s): 15.699
 +==============================================================+ 
 |                   RESULTADOS DOS EXPERIMENTOS                | 
 +==============================================================+ 
 | Função Objetivo:
 | 	 Média:                 -210.015
 | 	 Devio-Padrão:          +7.398
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +119.985
 | 	 Desvio-Padrão:         +7.398
 | Tempo de Execução: 
 | 	 Média:                 +15.652
 | 	 Devio-Padrão:          +0.021
 +==============================================================+ 

