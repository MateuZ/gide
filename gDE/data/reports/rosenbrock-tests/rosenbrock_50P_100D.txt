 +==============================================================+ 
 |                          CONFIGURAÇÃO                        |
 +==============================================================+ 
 | Número de execuções:      30
 | Tamanho da População:     50
 | Número de Gerações:       20000
 | Número de Dimensões:      100
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.30
 | Função Objetivo:          shifted_rosenbrock
 +==============================================================+ 
 | Execução: 1  Melhor Global: +470.692(80.692) Tempo de Execução (s): 47.697
 | Execução: 2  Melhor Global: +476.376(86.376) Tempo de Execução (s): 47.667
 | Execução: 3  Melhor Global: +476.970(86.970) Tempo de Execução (s): 47.612
 | Execução: 4  Melhor Global: +473.748(83.748) Tempo de Execução (s): 47.675
 | Execução: 5  Melhor Global: +473.835(83.835) Tempo de Execução (s): 47.655
 | Execução: 6  Melhor Global: +477.797(87.797) Tempo de Execução (s): 47.667
 | Execução: 7  Melhor Global: +472.948(82.948) Tempo de Execução (s): 47.605
 | Execução: 8  Melhor Global: +477.371(87.371) Tempo de Execução (s): 47.676
 | Execução: 9  Melhor Global: +476.619(86.619) Tempo de Execução (s): 47.654
 | Execução: 10 Melhor Global: +476.988(86.988) Tempo de Execução (s): 47.673
 | Execução: 11 Melhor Global: +472.566(82.566) Tempo de Execução (s): 47.620
 | Execução: 12 Melhor Global: +477.796(87.796) Tempo de Execução (s): 47.677
 | Execução: 13 Melhor Global: +472.642(82.642) Tempo de Execução (s): 47.625
 | Execução: 14 Melhor Global: +477.469(87.469) Tempo de Execução (s): 47.671
 | Execução: 15 Melhor Global: +474.209(84.209) Tempo de Execução (s): 47.626
 | Execução: 16 Melhor Global: +478.205(88.205) Tempo de Execução (s): 47.676
 | Execução: 17 Melhor Global: +477.232(87.232) Tempo de Execução (s): 47.657
 | Execução: 18 Melhor Global: +530.561(140.561) Tempo de Execução (s): 47.636
 | Execução: 19 Melhor Global: +528.834(138.834) Tempo de Execução (s): 47.626
 | Execução: 20 Melhor Global: +477.857(87.857) Tempo de Execução (s): 47.678
 | Execução: 21 Melhor Global: +476.870(86.870) Tempo de Execução (s): 47.661
 | Execução: 22 Melhor Global: +471.110(81.110) Tempo de Execução (s): 47.648
 | Execução: 23 Melhor Global: +474.068(84.068) Tempo de Execução (s): 47.623
 | Execução: 24 Melhor Global: +527.597(137.597) Tempo de Execução (s): 47.678
 | Execução: 25 Melhor Global: +471.455(81.455) Tempo de Execução (s): 47.721
 | Execução: 26 Melhor Global: +473.105(83.105) Tempo de Execução (s): 47.659
 | Execução: 27 Melhor Global: +475.307(85.307) Tempo de Execução (s): 47.639
 | Execução: 28 Melhor Global: +477.984(87.984) Tempo de Execução (s): 47.683
 | Execução: 29 Melhor Global: +527.958(137.958) Tempo de Execução (s): 47.660
 | Execução: 30 Melhor Global: +472.313(82.313) Tempo de Execução (s): 47.669
 +==============================================================+ 
 |                   RESULTADOS DOS EXPERIMENTOS                | 
 +==============================================================+ 
 | Função Objetivo:
 | 	 Média:                 +482.283
 | 	 Devio-Padrão:          +18.365
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +92.283
 | 	 Desvio-Padrão:         +18.365
 | Tempo de Execução: 
 | 	 Média:                 +47.657
 | 	 Devio-Padrão:          +0.026
 +==============================================================+ 

