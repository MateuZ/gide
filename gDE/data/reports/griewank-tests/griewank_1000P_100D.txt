 +==============================================================+ 
 |                          CONFIGURAÇÃO                        |
 +==============================================================+ 
 | Número de execuções:      30
 | Tamanho da População:     1000
 | Número de Gerações:       1000
 | Número de Dimensões:      100
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.30
 | Função Objetivo:          shifted_griewank
 +==============================================================+ 
 | Execução: 1  Melhor Global: -178.535(1.465) Tempo de Execução (s): 2.057
 | Execução: 2  Melhor Global: -178.576(1.424) Tempo de Execução (s): 2.024
 | Execução: 3  Melhor Global: -178.567(1.433) Tempo de Execução (s): 2.013
 | Execução: 4  Melhor Global: -178.532(1.468) Tempo de Execução (s): 2.001
 | Execução: 5  Melhor Global: -178.538(1.462) Tempo de Execução (s): 2.020
 | Execução: 6  Melhor Global: -178.521(1.479) Tempo de Execução (s): 2.014
 | Execução: 7  Melhor Global: -178.561(1.439) Tempo de Execução (s): 2.019
 | Execução: 8  Melhor Global: -178.555(1.445) Tempo de Execução (s): 2.012
 | Execução: 9  Melhor Global: -178.541(1.459) Tempo de Execução (s): 1.998
 | Execução: 10 Melhor Global: -178.505(1.495) Tempo de Execução (s): 2.015
 | Execução: 11 Melhor Global: -178.498(1.502) Tempo de Execução (s): 1.993
 | Execução: 12 Melhor Global: -178.557(1.443) Tempo de Execução (s): 2.008
 | Execução: 13 Melhor Global: -178.496(1.504) Tempo de Execução (s): 2.030
 | Execução: 14 Melhor Global: -178.542(1.458) Tempo de Execução (s): 2.022
 | Execução: 15 Melhor Global: -178.539(1.461) Tempo de Execução (s): 2.014
 | Execução: 16 Melhor Global: -178.531(1.469) Tempo de Execução (s): 2.002
 | Execução: 17 Melhor Global: -178.516(1.484) Tempo de Execução (s): 1.998
 | Execução: 18 Melhor Global: -178.544(1.456) Tempo de Execução (s): 2.023
 | Execução: 19 Melhor Global: -178.569(1.431) Tempo de Execução (s): 2.031
 | Execução: 20 Melhor Global: -178.502(1.498) Tempo de Execução (s): 2.015
 | Execução: 21 Melhor Global: -178.516(1.484) Tempo de Execução (s): 2.082
 | Execução: 22 Melhor Global: -178.560(1.440) Tempo de Execução (s): 2.017
 | Execução: 23 Melhor Global: -178.594(1.406) Tempo de Execução (s): 2.021
 | Execução: 24 Melhor Global: -178.524(1.476) Tempo de Execução (s): 2.042
 | Execução: 25 Melhor Global: -178.540(1.460) Tempo de Execução (s): 2.002
 | Execução: 26 Melhor Global: -178.529(1.471) Tempo de Execução (s): 2.017
 | Execução: 27 Melhor Global: -178.533(1.467) Tempo de Execução (s): 2.009
 | Execução: 28 Melhor Global: -178.517(1.483) Tempo de Execução (s): 2.022
 | Execução: 29 Melhor Global: -178.522(1.478) Tempo de Execução (s): 2.023
 | Execução: 30 Melhor Global: -178.541(1.459) Tempo de Execução (s): 2.091
 +==============================================================+ 
 |                   RESULTADOS DOS EXPERIMENTOS                | 
 +==============================================================+ 
 | Função Objetivo:
 | 	 Média:                 -178.537
 | 	 Devio-Padrão:          +0.023
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +1.463
 | 	 Desvio-Padrão:         +0.023
 | Tempo de Execução: 
 | 	 Média:                 +2.021
 | 	 Devio-Padrão:          +0.022
 +==============================================================+ 

