 +==============================================================+ 
 |                          CONFIGURAÇÃO                        |
 +==============================================================+ 
 | Número de execuções:      30
 | Tamanho da População:     50
 | Número de Gerações:       10000
 | Número de Dimensões:      50
 | Fator de Mutação (F):     0.50
 | Taxa de Crossover:        0.30
 | Função Objetivo:          shifted_griewank
 +==============================================================+ 
 | Execução: 1  Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.824
 | Execução: 2  Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.744
 | Execução: 3  Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.746
 | Execução: 4  Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.744
 | Execução: 5  Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.947
 | Execução: 6  Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.932
 | Execução: 7  Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.803
 | Execução: 8  Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.742
 | Execução: 9  Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.747
 | Execução: 10 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.912
 | Execução: 11 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.747
 | Execução: 12 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.750
 | Execução: 13 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.750
 | Execução: 14 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.745
 | Execução: 15 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.748
 | Execução: 16 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.745
 | Execução: 17 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.765
 | Execução: 18 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.754
 | Execução: 19 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.740
 | Execução: 20 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.739
 | Execução: 21 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.744
 | Execução: 22 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.738
 | Execução: 23 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.742
 | Execução: 24 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.741
 | Execução: 25 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.746
 | Execução: 26 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.740
 | Execução: 27 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.780
 | Execução: 28 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.744
 | Execução: 29 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.743
 | Execução: 30 Melhor Global: -180.000(0.000) Tempo de Execução (s): 15.799
 +==============================================================+ 
 |                   RESULTADOS DOS EXPERIMENTOS                | 
 +==============================================================+ 
 | Função Objetivo:
 | 	 Média:                 -180.000
 | 	 Devio-Padrão:          +0.000
 | Erro Função Objetivo (EFV)
 | 	 Média:                 +0.000
 | 	 Desvio-Padrão:         +0.000
 | Tempo de Execução: 
 | 	 Média:                 +15.771
 | 	 Devio-Padrão:          +0.057
 +==============================================================+ 

