#ifndef __DE__
#define __DE__

/* C++ includes */
#include <tuple>
#include <vector>
#include <fstream>
#include <string>
#include <iostream>
#include <iomanip>
#include <sys/time.h>

/* My libs includes */
#include "Individuo.hpp"
#include "ShiftedGriewank.cuh"
#include "ShiftedSphere.cuh"
#include "ShiftedRastrigin.cuh"
#include "ShiftedRosenbrock.cuh"

/* CUDA includes */
#include <thrust/sequence.h>
#include <cuda.h>
#include <curand_kernel.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/execution_policy.h>

typedef std::vector< int > vi;
typedef std::vector< Individual > vS;

typedef thrust::device_vector<int> int_dV;
typedef thrust::device_vector<int>::iterator int_dV_it;

typedef thrust::device_vector<double> double_dV;
typedef thrust::device_vector<double>::iterator double_dV_it;

#define CUDA_CALL(x) do { if((x) !=  cudaSuccess) { \
printf ("Error  at %s:%d\n",__FILE__ ,__LINE__); \
return  EXIT_FAILURE ;}}  while (0)

double stime();

int rand_btw(int a, int b);

__global__ void index_generator(int *v, int *vf, uint pop_size);

std::tuple<double, double, double> DE_execute(
  uint pop_size, uint n_ger, uint n_dim,
  double mf, double cr,
  std::string FuncObj
);

__global__ void DE_ShiftedGriewank(
	int *vf,
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
	double F, double CR, uint pop_size, uint n_dim
);

__global__ void DE_ShiftedRastrigin(
	int *vf,
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
	double F, double CR, uint pop_size, uint n_dim
);

__global__ void DE_ShiftedSphere(
	int *vf,
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
	double F, double CR, uint pop_size, uint n_dim
);

__global__ void DE_ShiftedRosenbrock(
	int *vf,
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
	double F, double CR, uint pop_size, uint n_dim
);

__global__ void subs_better_offspring(
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
  uint pop_size, uint n_dim
);

#endif
