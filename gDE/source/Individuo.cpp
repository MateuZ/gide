#include "Individuo.hpp"

Individual::Individual()
{
	gene = NULL;
  evaluated_value = 0.0f;
}

Individual::~Individual()
{
  if( gene != NULL ){
    delete [] gene;
    gene = NULL;
  }
}

void Individual::init( const uint size, const double LB, const double UB )
{
  gene = new double[size];

  std::mt19937 rng;
  rng.seed(std::random_device{}());
	std::uniform_real_distribution<double> d(LB, UB);

	for( int i = 0; i < size; i++ )
		gene[i] = d(rng);
}

void Individual::clean_init( const uint size )
{
	gene = new double[size];
	std::fill_n(gene, size, static_cast<double>(0.0));
}

void Individual::print( const uint size )
{
  printf("---------------------------------------------\n");

  for( int i = 0; i < size; i++ )
    printf("[%d]: %+.10lf\n", i, gene[i]);

  printf("---------------------------------------------\n");
}

void Individual::copy_data( const Individual& b, const uint size )
{
  memcpy(gene, b.gene, sizeof(double) * size);
	evaluated_value = b.evaluated_value;
}
