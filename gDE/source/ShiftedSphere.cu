#include "ShiftedSphere.cuh"

__host__ __device__ ShiftedSphere::ShiftedSphere()
{
	m_lowerBound = -100;
	m_upperBound =  100;
	m_fBias = -450.0;
}

__host__ __device__  ShiftedSphere::~ShiftedSphere(){
	/* empty*/
}

__host__ __device__  double ShiftedSphere::getOptimalPoint()
{
	return -450.0;
};

double ShiftedSphere::evaluate(double individual[], int size)
{
	double z = 0;
	double Fx = 0;
	unsigned short int i;
	for (i = 0; i < size; i++)
        {
        	z = individual[i] - sphere[i];
	        Fx += z*z;
        }
	Fx = Fx + m_fBias;
	if(Fx - getOptimalPoint() <= 10e-8)
	{
		Fx = getOptimalPoint();
	}
	return Fx;
}

std::string ShiftedSphere::getName()
{
	return "ShiftedSphere";
}


__host__ __device__  double ShiftedSphere::verifyBounds(double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

__host__ __device__  double ShiftedSphere::getLowerBound()
{
	return m_lowerBound;
}

__host__ __device__ double ShiftedSphere::getUpperBound()
{
	return m_upperBound;
}
