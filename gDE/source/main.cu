/* C++ includes */

#include <iostream>
#include <chrono>
#include <functional>
#include <iomanip>
#include <random>
#include <algorithm>
#include <string>

/* C includes */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <assert.h>

#include <boost/program_options.hpp>

#include "DE.cuh"

namespace po = boost::program_options;

void show_params(
	uint n_runs, uint pop_size,
	uint n_ger, uint n_dimensions,
	double mutation_factor,
	double crossover_rate,
	std::string FuncObj
){
	printf(" | Número de execuções:      %d\n", n_runs);
	printf(" | Tamanho da População:     %d\n", pop_size);
	printf(" | Número de Gerações:       %d\n", n_ger);
	printf(" | Número de Dimensões:      %d\n", n_dimensions);
	printf(" | Fator de Mutação (F):     %.2lf\n", mutation_factor);
	printf(" | Taxa de Crossover:        %.2lf\n", crossover_rate);
	printf(" | Função Objetivo:          %s\n", FuncObj.c_str());
}

int main(int argc, char *argv[])
{
	srand(time(NULL));
	uint n_runs, pop_size, n_ger, n_dim;
	double m_f, cr;
	std::string FuncObj;

	try{
		po::options_description config("Opções");
		config.add_options()
			("runs,r"    , po::value<uint>(&n_runs)->default_value(5)    , "Número de Execuções" )
			("pop_size,p", po::value<uint>(&pop_size)->default_value(100), "Tamanho da População")
			("n_ger,g"   , po::value<uint>(&n_ger)->default_value(1000)  , "Número de Gerações"  )
			("dim,d"     , po::value<uint>(&n_dim)->default_value(10)    , "Número de Dimensões" )
			("mf,f"      , po::value<double>(&m_f)->default_value(0.5f)  , "Fator de Mutação"    )
			("cr,c"      , po::value<double>(&cr)->default_value(0.90f)  , "Taxa de Crossover"   )
			("func_obj,o", po::value<std::string>(&FuncObj)->default_value("shifted_sphere"), "Função Objetivo")
			("help,h", "Mostrar texto de Ajuda");

		po::options_description cmdline_options;
		cmdline_options.add(config);
		po::variables_map vm;
		store(po::command_line_parser(argc, argv).options(cmdline_options).run(), vm);
		notify(vm);
		if (vm.count("help")) {
			std::cout << cmdline_options << "\n";
			return 0;
		}
	}catch(std::exception& e){
		std::cout << e.what() << "\n";
		return 1;
	}
	if( FuncObj != "shifted_sphere" && FuncObj != "shifted_rosenbrock"
			&& FuncObj != "shifted_griewank" && FuncObj != "shifted_rastrigin"){
			std::cout << "The algorithm does not support the received method." << std::endl;
			std::cout << "Methods supported: " << std::endl;
			std::cout << " - shifted_sphere\n";
			std::cout << " - shifted_rosenbrock\n";
			std::cout << " - shifted_griewank\n";
			std::cout << " - shifted_rastrigin\n";
			return 1;
	}
	printf(" +==============================================================+ \n");
	printf(" |                          CONFIGURAÇÃO                        |\n");
	printf(" +==============================================================+ \n");
	show_params(n_runs, pop_size, n_ger, n_dim, m_f, cr, FuncObj);

	printf(" +==============================================================+ \n");

	//get the best global and the execution time
	std::vector< std::tuple<double, double, double> > stats;
	std::tuple<double, double, double> run_stats;
	for( int i = 1; i <= n_runs; i++ ){
		run_stats = DE_execute(pop_size, n_ger, n_dim, m_f, cr, FuncObj);

		stats.push_back(run_stats);

		printf(" | Execução: %-2d Melhor Global: %+.3lf(%.3lf) Tempo de Execução (s): %.3lf\n",
		 i, std::get<0>(run_stats), std::get<2>(run_stats), std::get<1>(run_stats));
	}

	double FO_mean = 0.0f, FO_std = 0.0f, T_mean = 0.0f, T_std = 0.0f, EFV_mean, EFV_std;
	for( auto it = stats.begin(); it != stats.end(); it++){
		FO_mean += std::get<0>(*it);
		T_mean  += std::get<1>(*it);
		EFV_mean+= std::get<2>(*it);
	}
	FO_mean /= n_runs;
	T_mean  /= n_runs;
	EFV_mean /= n_runs;
	for( auto it = stats.begin(); it != stats.end(); it++){
		FO_std += (( std::get<0>(*it) - FO_mean )*( std::get<0>(*it) - FO_mean ));
		T_std  += (( std::get<1>(*it) - T_mean  )*( std::get<1>(*it) - T_mean  ));
		EFV_std+= (( std::get<2>(*it) - EFV_mean)*( std::get<2>(*it) - EFV_mean));
	}
	FO_std /= n_runs;
	FO_std = sqrt(FO_std);
	T_std /= n_runs;
	T_std = sqrt(T_std);
	EFV_std /= n_runs;
	EFV_std = sqrt(EFV_std);
	printf(" +==============================================================+ \n");
	printf(" |                   RESULTADOS DOS EXPERIMENTOS                | \n");
	printf(" +==============================================================+ \n");
	printf(" | Função Objetivo:\n");
	printf(" | \t Média:                 %+.3lf\n", FO_mean);
	printf(" | \t Devio-Padrão:          %+.3lf\n", FO_std);
	printf(" | Erro Função Objetivo (EFV)\n");
	printf(" | \t Média:                 %+.3lf\n", EFV_mean);
	printf(" | \t Desvio-Padrão:         %+.3lf\n", EFV_std);
	printf(" | Tempo de Execução: \n");
	printf(" | \t Média:                 %+.3lf\n", T_mean);
	printf(" | \t Devio-Padrão:          %+.3lf\n", T_std);
	printf(" +==============================================================+ \n");
	return 0;
}
