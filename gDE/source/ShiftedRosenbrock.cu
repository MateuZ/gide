#include "ShiftedRosenbrock.cuh"

__host__ __device__ ShiftedRosenbrock::ShiftedRosenbrock(){
  m_lowerBound = -100.0f;
  m_upperBound = +100.0f;
  m_fBias = 390.0;
}

__host__ __device__ ShiftedRosenbrock::~ShiftedRosenbrock(){
  /* empty */
}

__host__ __device__ double ShiftedRosenbrock::getOptimalPoint(){
  return 390.0;
}

double ShiftedRosenbrock::evaluate(double individual[], int size)
{
	double zx[size];
	double Fx = 0;
	unsigned short int i;
	for(i = 0; i < size; i++)
	{
		zx[i] = individual[i] - rosenbrock[i] + 1;
	}

	for(i = 0; i < size-1; i++)
	{
	        Fx = Fx + 100*( pow((pow(zx[i],2)-zx[i+1]) , 2) ) + pow((zx[i]-1) , 2);
	}
	Fx =  Fx + m_fBias;
	if(Fx - getOptimalPoint() <= 10e-8)
	{
		Fx = getOptimalPoint();
	}
	return Fx;
}

std::string ShiftedRosenbrock::getName()
{
	return "ShiftedRosenbrock";
}

__host__ __device__ double ShiftedRosenbrock::verifyBounds( double d)
{
	if(d < m_lowerBound)
		return m_lowerBound;
	if(d > m_upperBound)
		return m_upperBound;
	return d;
}

double ShiftedRosenbrock::getLowerBound()
{
	return m_lowerBound;
}

double ShiftedRosenbrock::getUpperBound()
{
	return m_upperBound;
}
