#include "DE.cuh"

double stime(){
    struct timeval tv;
    struct timezone tz;
    gettimeofday(&tv, &tz);
    double mlsec = 1000.0 * ((double)tv.tv_sec + (double)tv.tv_usec/1000000.0);
    return mlsec/1000.0;
}

int rand_btw(int a, int b){
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<int> dist(a, b);
	return dist(gen);
}

__global__ void index_generator(int *v, int *vf, uint pop_size){
  int i = threadIdx.x + blockDim.x * blockIdx.x;

  curandState s;
  int n1, n2, n3;
  curand_init((i + 1) * clock(), 0, 0, &s);

  n1 = curand(&s) % pop_size;

  vf[i] = v[n1];
  if( v[n1] == i ){
    n1 = (n1 + 1) % pop_size;
    vf[i] = v[n1];
  }

  n2 = (curand(&s) % (int)pop_size/3) + 1;

  vf[i + pop_size] = v[(n1 + n2) % pop_size];
  if( vf[i + pop_size] == i ){
    n2 = (n2 + 1) % pop_size;
    vf[i + pop_size] = v[(n1 + n2) % pop_size];
  }

  n3 = (curand(&s) % (int)pop_size/3) + 1;

  vf[i + 2 * pop_size] = v[(n1 + n2 + n3) % pop_size];
  if( vf[i + 2 * pop_size] == i ){
    n3 = (n3 + 1) % pop_size;
    vf[i + 2 * pop_size] = v[(n1 + n2 + n3) % pop_size];
  }
}

__global__ void subs_better_offspring(
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
  uint pop_size, uint n_dim
){
  int index = threadIdx.x + blockDim.x * blockIdx.x;
  if(aux_evals[index] <= pop_evals[index]){
    for( int i = 0; i < n_dim; i++ ){
      pop_genes[ (index * n_dim) + i ] = aux_genes[ (index * n_dim) + i ];
    }
    pop_evals[index] = aux_evals[index];
  }
}

__global__ void DE_ShiftedGriewank(
	int *vf,
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
	double F, double CR, uint pop_size, uint n_dim//,
){
	int index = threadIdx.x + blockDim.x * blockIdx.x;

	int n1, n2, n3, rnbr;
  ShiftedGriewank sg;
  double z = 0;
  double top1 = 0;
  double top2 = 0;

  top1 = 0.0;
  top2 = 1.0;

  n1 = vf[index];
  n2 = vf[index + pop_size];
  n3 = vf[index + pop_size * 2];
  //printf("(n1) %d (n2) %d (n3) %d\n", n1, n2, n3);

	curandState s;
	curand_init((index + 1) * clock(), 0, 0, &s);

	rnbr = curand(&s)%n_dim;
	for( int k = 0; k < n_dim; k++ ){
		if( (curand_uniform(&s) <= CR) or (k == n_dim-1) ){

      aux_genes[(index * n_dim) + rnbr] = pop_genes[(n3 * n_dim) + rnbr] + F * (pop_genes[(n1 * n_dim) + rnbr] - pop_genes[(n2 * n_dim) + rnbr]);

			if( aux_genes[(index * n_dim) + rnbr] > +600.0f )
				aux_genes[(index * n_dim) + rnbr] = +600.0f;

      if( aux_genes[(index * n_dim) + rnbr] < -600.0f)
  			aux_genes[(index * n_dim) + rnbr] = -600.0f;

		}else{
			//receive an equal dimension;
			aux_genes[(index * n_dim) + rnbr] = pop_genes[(index * n_dim) + rnbr];
		}
    z = aux_genes[(index * n_dim) + rnbr] - sg.griewank[rnbr];
    top1 = top1 + static_cast<double>(( pow(z,2) / 4000 ));
    top2 = static_cast<double>(top2 * ( cos(z/sqrt(static_cast<double>(rnbr+1)))));

    rnbr = (rnbr+1)%n_dim;
	}

	top1 =  (top1 - top2 + 1.0 + (-180.0));
	if(top1 - sg.getOptimalPoint() <= 10e-8)	{
		top1 = sg.getOptimalPoint();
	}
	aux_evals[index] = top1;
}

__global__ void DE_ShiftedRosenbrock(
	int *vf,
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
	double F, double CR, uint pop_size, uint n_dim
){
	int index = threadIdx.x + blockDim.x * blockIdx.x;

	int n1, n2, n3, rnbr;

  ShiftedRosenbrock sr;
  double *zx = new double[n_dim];
  double Fx = 0.0;

  n1 = vf[index];
  n2 = vf[index + pop_size];
  n3 = vf[index + pop_size * 2];
  //printf("(n1) %d (n2) %d (n3) %d\n", n1, n2, n3);

	curandState s;
	curand_init((index + 1) * clock(), 0, 0, &s);

	rnbr = curand(&s)%n_dim;
	for( int k = 0; k < n_dim; k++ ){
		if( (curand_uniform(&s) <= CR) or (k == n_dim-1) ){

      aux_genes[(index * n_dim) + rnbr] = pop_genes[(n3 * n_dim) + rnbr] + F * (pop_genes[(n1 * n_dim) + rnbr] - pop_genes[(n2 * n_dim) + rnbr]);

			if( aux_genes[(index * n_dim) + rnbr] > +600.0f )
				aux_genes[(index * n_dim) + rnbr] = +600.0f;

      if( aux_genes[(index * n_dim) + rnbr] < -600.0f)
  			aux_genes[(index * n_dim) + rnbr] = -600.0f;

		}else{
			//receive an equal dimension;
			aux_genes[(index * n_dim) + rnbr] = pop_genes[(index * n_dim) + rnbr];
		}
    zx[rnbr] = static_cast<double>(aux_genes[(index * n_dim) + rnbr] - sr.rosenbrock[rnbr] + 1.0);
    rnbr = (rnbr+1)%n_dim;
	}

  for( int i = 0; i < n_dim - 1; i++ ){
    Fx += static_cast<double>(100.0*( pow((pow(zx[i],2)-zx[i+1]) , 2) ) + pow((zx[i]-1) , 2));
  }
	Fx += sr.m_fBias;

	if(Fx - sr.getOptimalPoint() <= 10e-8)	{
		Fx = sr.getOptimalPoint();
	}
	aux_evals[index] = Fx;
  free(zx);
}

__global__ void DE_ShiftedRastrigin(
	int *vf,
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
	double F, double CR, uint pop_size, uint n_dim//,
){
	int index = threadIdx.x + blockDim.x * blockIdx.x;

	int n1, n2, n3, rnbr;
  ShiftedRastrigin sr;
  double z = 0;
	double Fx = 0;

  n1 = vf[index];
  n2 = vf[index + pop_size];
  n3 = vf[index + pop_size * 2];

	curandState s;
	curand_init((index + 1) * clock(), 0, 0, &s);

	rnbr = curand(&s)%n_dim;
	for( int k = 0; k < n_dim; k++ ){
		if( (curand_uniform(&s) <= CR) or (k == n_dim-1) ){

      aux_genes[(index * n_dim) + rnbr] = pop_genes[(n3 * n_dim) + rnbr] + F * (pop_genes[(n1 * n_dim) + rnbr] - pop_genes[(n2 * n_dim) + rnbr]);

			if( aux_genes[(index * n_dim) + rnbr] > sr.getUpperBound() )
				aux_genes[(index * n_dim) + rnbr] = sr.getUpperBound();

      if( aux_genes[(index * n_dim) + rnbr] < sr.getLowerBound() )
  			aux_genes[(index * n_dim) + rnbr] = sr.getLowerBound();

		}else{
			//receive an equal dimension;
			aux_genes[(index * n_dim) + rnbr] = pop_genes[(index * n_dim) + rnbr];
		}
    z = aux_genes[(index * n_dim) + rnbr] - sr.rastrigin[rnbr];
    Fx = Fx + static_cast<double>(( pow(z, 2) - 10.0*cos(2.0*sr.PI*z) + 10.0 ));

    rnbr = (rnbr+1)%n_dim;
	}
  Fx =  Fx + sr.m_fBias;
  if(Fx - sr.getOptimalPoint() <= 10e-8)
		Fx = sr.getOptimalPoint();

	aux_evals[index] = Fx;
}

__global__ void DE_ShiftedSphere(
	int *vf,
  double *pop_genes, double *pop_evals,
  double *aux_genes, double *aux_evals,
	double F, double CR, uint pop_size, uint n_dim//,
){
  int index = threadIdx.x + blockDim.x * blockIdx.x;

	int n1, n2, n3, rnbr;
  ShiftedSphere ss;
  double z = 0;
	double Fx = 0;

  n1 = vf[index];
  n2 = vf[index + pop_size];
  n3 = vf[index + pop_size * 2];

	curandState s;
	curand_init((index + 1) * clock(), 0, 0, &s);

	rnbr = curand(&s)%n_dim;
	for( int k = 0; k < n_dim; k++ ){
		if( (curand_uniform(&s) <= CR) or (k == n_dim-1) ){

      aux_genes[(index * n_dim) + rnbr] = pop_genes[(n3 * n_dim) + rnbr] + F * (pop_genes[(n1 * n_dim) + rnbr] - pop_genes[(n2 * n_dim) + rnbr]);

			if( aux_genes[(index * n_dim) + rnbr] > ss.getUpperBound() )
				aux_genes[(index * n_dim) + rnbr] = ss.getUpperBound();

      if( aux_genes[(index * n_dim) + rnbr] < ss.getLowerBound())
  			aux_genes[(index * n_dim) + rnbr] = ss.getLowerBound();

		}else{
			//receive an equal dimension;
			aux_genes[(index * n_dim) + rnbr] = pop_genes[(index * n_dim) + rnbr];
		}
    z = aux_genes[(index * n_dim) + rnbr] - ss.sphere[rnbr];
    Fx += z*z;

    rnbr = (rnbr+1)%n_dim;
	}
  Fx =  Fx + ss.m_fBias;
  if(Fx - ss.getOptimalPoint() <= 10e-8)
		Fx = ss.getOptimalPoint();

	aux_evals[index] = Fx;
}

__global__ void griewank_eval(
  double *aux_genes, double *aux_evals,
  uint pop_size, uint n_dim)
{

  int index = threadIdx.x + blockDim.x * blockIdx.x;

  ShiftedGriewank sg;
  double z = 0;
	double top1 = 0;
	double top2 = 0;
	unsigned short int i;

	top1 = 0.0;
	top2 = 1.0;
	for(i = 0; i < n_dim; i++){
		z = aux_genes[(index * n_dim) + i] - sg.griewank[i];
		top1 = top1 + static_cast<double>(( pow(z,2) / 4000 ));
		top2 = static_cast<double>(top2 * ( cos(z/sqrt(static_cast<double>(i+1)))));
	}
	top1 =  (top1 - top2 + 1.0 + (-180.0));
	if(top1 - sg.getOptimalPoint() <= 10e-20)	{
		top1 = sg.getOptimalPoint();
	}
	aux_evals[index] = top1;
}

__global__ void rastrigin_eval(
  double *aux_genes, double *aux_evals,
  uint pop_size, uint n_dim)
{

  int index = threadIdx.x + blockDim.x * blockIdx.x;

  ShiftedRastrigin sg;
  double z = 0;
	double Fx = 0;

	for(int i = 0; i < n_dim; i++){
		z = aux_genes[(index * n_dim) + i] - sg.rastrigin[i];
    Fx = Fx + ( pow(z,2) - 10*cos(2*sg.PI*z) + 10);
	}
	Fx =  Fx + sg.m_fBias;
	if( Fx - sg.getOptimalPoint() <= 10e-8)	{
		Fx = sg.getOptimalPoint();
	}
	aux_evals[index] = Fx;
}

__global__ void rosenbrock_eval(
  double *aux_genes, double *aux_evals,
  uint pop_size, uint n_dim)
{
  int index = threadIdx.x + blockDim.x * blockIdx.x;

  double *zx = new double[n_dim];
  double Fx = 0.0f;

  ShiftedRosenbrock sg;

  for(int i = 0; i < n_dim; i++)
    zx[i] = aux_genes[((index * n_dim) + i)] - sg.rosenbrock[i] + 1;

	for(int i = 0; i < n_dim-1; i++)
		Fx += 100*( pow((pow(zx[i],2)-zx[i+1]) , 2) ) + pow((zx[i]-1) , 2);

	Fx += sg.m_fBias;
	if(Fx - sg.getOptimalPoint() <= 10e-8)	{
		Fx = sg.getOptimalPoint();
	}
	aux_evals[index] = Fx;

  free(zx);
}

__global__ void sphere_eval(
  double *aux_genes, double *aux_evals,
  uint pop_size, uint n_dim)
{

  int index = threadIdx.x + blockDim.x * blockIdx.x;

  ShiftedSphere sg;
  double z = 0;
	double Fx = 0.0f;

	for(int i = 0; i < n_dim; i++){
		z = aux_genes[(index * n_dim) + i] - sg.sphere[i];
		Fx += z*z;
	}
	Fx += sg.m_fBias;
	if(Fx - sg.getOptimalPoint() <= 10e-8)	{
		Fx = sg.getOptimalPoint();
	}
	aux_evals[index] = Fx;
}

std::tuple<double, double, double> DE_execute(
  uint pop_size, uint n_ger, uint n_dim,
  double mf, double cr,
  std::string FuncObj
){

  int n_blocks = 25;
  int n_threads = 40;
  /* GPU */
  int_dV v_gpu(pop_size);
  int_dV vf_gpu(pop_size * 3);
  /*
   * Similar to C++ STL function std::iota
   * Reference: https://goo.gl/mcpZfs
  */
  int_dV_it it = v_gpu.begin();
  thrust::sequence(thrust::device, it, it + pop_size);

  double_dV pop_genes(pop_size * n_dim);
  double_dV pop_evals(pop_size);
  double_dV aux_genes(pop_size * n_dim);
  double_dV aux_evals(pop_size);
  thrust::fill(thrust::device, pop_evals.begin(), pop_evals.end(), 0.0f);
  thrust::fill(thrust::device, aux_evals.begin(), aux_evals.end(), 0.0f);
  thrust::fill(thrust::device, aux_genes.begin(), aux_genes.end(), 0.0f);

  double best, EFV;
  float time;

  if( FuncObj == "shifted_griewank"){

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    thrust::host_vector<double> hv(pop_size * n_dim);

    std::mt19937 rng;
    rng.seed(std::random_device{}());
    std::uniform_real_distribution<double> d(-600.0, +600.00);

    for( int i = 0; i < (pop_size * n_dim); i++ )
      hv[i] = d(rng);

    pop_genes = hv;

    ShiftedGriewank sg;

    cudaEventRecord(start);

    griewank_eval<<<n_blocks, n_threads>>>(
      thrust::raw_pointer_cast(&pop_genes[0]),
      thrust::raw_pointer_cast(&pop_evals[0]),
      pop_size, n_dim
    );
    for(int i = 0; i < n_ger; i++){
      index_generator<<<n_blocks, n_threads>>>(thrust::raw_pointer_cast(&v_gpu[0]), thrust::raw_pointer_cast(&vf_gpu[0]), pop_size);
      DE_ShiftedGriewank<<<n_blocks, n_threads>>>(
        thrust::raw_pointer_cast(&vf_gpu[0]),
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        mf, cr, pop_size, n_dim
      );
      subs_better_offspring<<<n_blocks, n_threads>>>(
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        pop_size, n_dim);
    }
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    cudaEventElapsedTime(&time, start, stop);
    time /= 1000.0f;
    double_dV_it it = pop_evals.begin();
    best = static_cast<double>(*it);
    for(; it != pop_evals.end(); it++){
      if( static_cast<double>(*it) < best ){
        best = static_cast<double>(*it);
      }
    }
    EFV = fabs(fabs(best) - 180.00f);
  }
  else if( FuncObj == "shifted_sphere" ){
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    thrust::host_vector<double> hv(pop_size * n_dim);

    std::mt19937 rng;
    rng.seed(std::random_device{}());
    std::uniform_real_distribution<double> d(-100.00, +100.00);

    for( int i = 0; i < (pop_size * n_dim); i++ )
      hv[i] = d(rng);

    pop_genes = hv;

    cudaEventRecord(start);

    sphere_eval<<<n_blocks, n_threads>>>(
      thrust::raw_pointer_cast(&pop_genes[0]),
      thrust::raw_pointer_cast(&pop_evals[0]),
      pop_size, n_dim
    );
    for(int i = 0; i < n_ger; i++){
      index_generator<<<n_blocks, n_threads>>>(thrust::raw_pointer_cast(&v_gpu[0]), thrust::raw_pointer_cast(&vf_gpu[0]), pop_size);
      DE_ShiftedSphere<<<n_blocks, n_threads>>>(
        thrust::raw_pointer_cast(&vf_gpu[0]),
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        mf, cr, pop_size, n_dim
      );
      subs_better_offspring<<<n_blocks, n_threads>>>(
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        pop_size, n_dim);
    }
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    cudaEventElapsedTime(&time, start, stop);
    time /= 1000.0f;
    double_dV_it it = pop_evals.begin();
    best = static_cast<double>(*it);
    for(; it != pop_evals.end(); it++){
      if( static_cast<double>(*it) < best ){
        best = static_cast<double>(*it);
      }
    }
    EFV = fabs(fabs(best) - 450.00f);
  }
  else if( FuncObj == "shifted_rastrigin" )
  {
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    thrust::host_vector<double> hv(pop_size * n_dim);

    std::mt19937 rng;
    rng.seed(std::random_device{}());
    std::uniform_real_distribution<double> d(-5.12, +5.12);

    for( int i = 0; i < (pop_size * n_dim); i++ )
      hv[i] = d(rng);

    pop_genes = hv;

    cudaEventRecord(start);

    rastrigin_eval<<<n_blocks, n_threads>>>(
      thrust::raw_pointer_cast(&pop_genes[0]),
      thrust::raw_pointer_cast(&pop_evals[0]),
      pop_size, n_dim
    );
    for(int i = 0; i < n_ger; i++){
      index_generator<<<n_blocks, n_threads>>>(thrust::raw_pointer_cast(&v_gpu[0]), thrust::raw_pointer_cast(&vf_gpu[0]), pop_size);
      DE_ShiftedRastrigin<<<n_blocks, n_threads>>>(
        thrust::raw_pointer_cast(&vf_gpu[0]),
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        mf, cr, pop_size, n_dim
      );
      subs_better_offspring<<<n_blocks, n_threads>>>(
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        pop_size, n_dim);
    }
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    cudaEventElapsedTime(&time, start, stop);
    time /= 1000.0f;
    double_dV_it it = pop_evals.begin();
    best = static_cast<double>(*it);
    for(; it != pop_evals.end(); it++){
      if( static_cast<double>(*it) < best ){
        best = static_cast<double>(*it);
      }
    }
    EFV = fabs(fabs(best) - 330.00f);
  }
  else if( FuncObj == "shifted_rosenbrock"){
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    thrust::host_vector<double> hv(pop_size * n_dim);

    std::mt19937 rng;
    rng.seed(std::random_device{}());
    std::uniform_real_distribution<double> d(-100.00, +100.00);

    for( int i = 0; i < (pop_size * n_dim); i++ )
      hv[i] = d(rng);

    pop_genes = hv;

    cudaEventRecord(start);

    rosenbrock_eval<<<n_blocks, n_threads>>>(
      thrust::raw_pointer_cast(&pop_genes[0]),
      thrust::raw_pointer_cast(&pop_evals[0]),
      pop_size, n_dim
    );
    for(int i = 0; i < n_ger; i++){
      index_generator<<<n_blocks, n_threads>>>(thrust::raw_pointer_cast(&v_gpu[0]), thrust::raw_pointer_cast(&vf_gpu[0]), pop_size);
      DE_ShiftedRosenbrock<<<n_blocks, n_threads>>>(
        thrust::raw_pointer_cast(&vf_gpu[0]),
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        mf, cr, pop_size, n_dim
      );
      subs_better_offspring<<<n_blocks, n_threads>>>(
        thrust::raw_pointer_cast(&pop_genes[0]),
        thrust::raw_pointer_cast(&pop_evals[0]),
        thrust::raw_pointer_cast(&aux_genes[0]),
        thrust::raw_pointer_cast(&aux_evals[0]),
        pop_size, n_dim);
    }
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);

    cudaEventElapsedTime(&time, start, stop);
    time /= 1000.0f;
    double_dV_it it = pop_evals.begin();
    best = static_cast<double>(*it);
    for(; it != pop_evals.end(); it++){
      if( static_cast<double>(*it) < best ){
        best = static_cast<double>(*it);
      }
    }
    EFV = fabs(fabs(best) - 390.00f);
  }
  /*CODE END*/
  return std::make_tuple(best, time, EFV);
}
